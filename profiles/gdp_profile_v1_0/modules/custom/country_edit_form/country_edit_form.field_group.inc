<?php
/**
 * @file
 * country_edit_form.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function country_edit_form_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_additional_indicators_gp|node|country|form';
  $field_group->group_name = 'group_additional_indicators_gp';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_additional_indicators';
  $field_group->data = array(
    'label' => 'Additional Indicators group',
    'weight' => '5',
    'children' => array(),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-additional-indicators-gp field-group-tabs ',
      ),
    ),
  );
  $export['group_additional_indicators_gp|node|country|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_additional_indicators|node|country|form';
  $field_group->group_name = 'group_additional_indicators';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Additional Indicators',
    'weight' => '11',
    'children' => array(
      0 => 'group_additional_indicators_gp',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-additional-indicators field-group-htab ',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_additional_indicators|node|country|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_agreements|node|country|form';
  $field_group->group_name = 'group_agreements';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Agreements',
    'weight' => '0',
    'children' => array(),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_agreements|node|country|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_attitudes_perceptions_gp|node|country|form';
  $field_group->group_name = 'group_attitudes_perceptions_gp';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_additional_indicators_gp';
  $field_group->data = array(
    'label' => 'Attitudes and perceptions',
    'weight' => '11',
    'children' => array(
      0 => 'iii_v_3_2',
      1 => 'iii_v_3_3',
      2 => 'iii_v_3_4',
      3 => 'iii_v_3_1',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_attitudes_perceptions_gp|node|country|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_country_tabs|node|country|form';
  $field_group->group_name = 'group_country_tabs';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Country Tabs',
    'weight' => '2',
    'children' => array(
      0 => 'group_additional_indicators',
      1 => 'group_agreements',
      2 => 'group_detention_centres',
      3 => 'group_immigration_indicators',
      4 => 'group_institutional_indicators',
      5 => 'group_legal_framework',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-country-tabs field-group-htabs ',
      ),
    ),
  );
  $export['group_country_tabs|node|country|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_detention_centres|node|country|form';
  $field_group->group_name = 'group_detention_centres';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_country_tabs';
  $field_group->data = array(
    'label' => 'Detention Centres',
    'weight' => '6',
    'children' => array(
      0 => 'field_dummy_field_5',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-detention-centres field-group-htab ',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_detention_centres|node|country|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_eco_and_development_gp|node|country|form';
  $field_group->group_name = 'group_eco_and_development_gp';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_additional_indicators_gp';
  $field_group->data = array(
    'label' => 'Economics and development ',
    'weight' => '10',
    'children' => array(
      0 => 'iii_v_2_1',
      1 => 'iii_v_2_2',
      2 => 'iii_v_2_3',
      3 => 'iii_v_2_4',
      4 => 'iii_v_2_5',
      5 => 'iii_v_2_6',
      6 => 'iii_v_2_7',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-eco-and-development-gp field-group-tab ',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_eco_and_development_gp|node|country|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_iii_iii_1|node|country|form';
  $field_group->group_name = 'group_iii_iii_1';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_instit_indicators_grp';
  $field_group->data = array(
    'label' => 'Institutions responsible for immigration detention',
    'weight' => '3',
    'children' => array(
      0 => 'iii_iii_1_1',
      1 => 'iii_iii_1_3',
      2 => 'iii_iii_1_4x',
      3 => 'field_apprehending_authorities',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-iii-iii-1 field-group-fieldset ',
      ),
    ),
  );
  $export['group_iii_iii_1|node|country|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_iii_iii_2|node|country|form';
  $field_group->group_name = 'group_iii_iii_2';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_instit_indicators_grp';
  $field_group->data = array(
    'label' => 'Detention monitoring institutions ',
    'weight' => '4',
    'children' => array(
      0 => 'iii_iii_2_3',
      1 => 'iii_iii_2_4',
      2 => 'iii_iii_2_5',
      3 => 'iii_iii_2_6',
      4 => 'iii_iii_2_7',
      5 => 'iii_iii_2_8',
      6 => 'iii_iii_2_9',
      7 => 'iii_iii_2_10',
      8 => 'iii_iii_2_11',
      9 => 'iii_iii_2_12',
      10 => 'iii_iii_2_13',
      11 => 'iii_iii_2_14',
      12 => 'iii_iii_2_15',
      13 => 'iii_iii_2_16',
      14 => 'iii_iii_2_17',
      15 => 'iii_iii_2_18',
      16 => 'iii_iii_2_19',
      17 => 'iii_iii_2_1',
      18 => 'iii_iii_2_2',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-iii-iii-2 field-group-tab ',
      ),
    ),
  );
  $export['group_iii_iii_2|node|country|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_iii_iv_1|node|country|form';
  $field_group->group_name = 'group_iii_iv_1';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_immigration_indicators_gp';
  $field_group->data = array(
    'label' => 'Detention and incarc stats group',
    'weight' => '14',
    'children' => array(
      0 => 'iii_iv_1_1',
      1 => 'iii_iv_1_2',
      2 => 'iii_iv_1_3',
      3 => 'iii_iv_1_4',
      4 => 'iii_iv_1_5',
      5 => 'iii_iv_1_6',
      6 => 'iii_iv_1_7',
      7 => 'iii_iv_1_8',
      8 => 'iii_iv_1_9',
      9 => 'iii_iv_1_10',
      10 => 'iii_iv_1_11',
      11 => 'iii_iv_1_12',
      12 => 'iii_iv_1_13',
      13 => 'iii_iv_1_14',
      14 => 'iii_iv_1_15',
      15 => 'iii_iv_1_16',
      16 => 'iii_iv_1_17',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_iii_iv_1|node|country|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_iii_iv_2|node|country|form';
  $field_group->group_name = 'group_iii_iv_2';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_immigration_indicators_gp';
  $field_group->data = array(
    'label' => 'Demographics and immigration-related statistics ',
    'weight' => '15',
    'children' => array(
      0 => 'iii_iv_2_1',
      1 => 'iii_iv_2_2',
      2 => 'iii_iv_2_3',
      3 => 'iii_iv_2_3_1',
      4 => 'iii_iv_2_4',
      5 => 'iii_iv_2_5',
      6 => 'iii_iv_2_6',
      7 => 'iii_iv_2_7',
      8 => 'iii_iv_2_8',
      9 => 'iii_iv_2_9',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-iii-iv-2 field-group-tab ',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_iii_iv_2|node|country|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_immigration_indicators_gp|node|country|form';
  $field_group->group_name = 'group_immigration_indicators_gp';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_immigration_indicators';
  $field_group->data = array(
    'label' => 'Immigration Group',
    'weight' => '13',
    'children' => array(
      0 => 'group_iii_iv_1',
      1 => 'group_iii_iv_2',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-immigration-indicators-gp field-group-tabs ',
      ),
    ),
  );
  $export['group_immigration_indicators_gp|node|country|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_immigration_indicators|node|country|form';
  $field_group->group_name = 'group_immigration_indicators';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Immigration Indicators',
    'weight' => '4',
    'children' => array(),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-immigration-indicators field-group-htab ',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_immigration_indicators|node|country|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_instit_indicators_grp|node|country|form';
  $field_group->group_name = 'group_instit_indicators_grp';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_institutional_indicators';
  $field_group->data = array(
    'label' => 'Institutional Indicators Group',
    'weight' => '2',
    'children' => array(),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-instit-indicators-grp field-group-tabs ',
      ),
    ),
  );
  $export['group_instit_indicators_grp|node|country|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_institutional_indicators|node|country|form';
  $field_group->group_name = 'group_institutional_indicators';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Institutional Indicators',
    'weight' => '8',
    'children' => array(
      0 => 'group_instit_indicators_grp',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-institutional-indicators field-group-htab ',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_institutional_indicators|node|country|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_legal_framework|node|country|form';
  $field_group->group_name = 'group_legal_framework';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Legal Framework',
    'weight' => '2',
    'children' => array(),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-legal-framework field-group-htab ',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_legal_framework|node|country|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_non_state_funding_gp|node|country|form';
  $field_group->group_name = 'group_non_state_funding_gp';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_instit_indicators_grp';
  $field_group->data = array(
    'label' => 'Non-state funding',
    'weight' => '6',
    'children' => array(
      0 => 'iii_iii_4_1',
      1 => 'iii_iii_4_2',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_non_state_funding_gp|node|country|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_outsourcing_and_privat_gp|node|country|form';
  $field_group->group_name = 'group_outsourcing_and_privat_gp';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_instit_indicators_grp';
  $field_group->data = array(
    'label' => 'Outsourcing and privatisation ',
    'weight' => '5',
    'children' => array(
      0 => 'iii_iii_3_1',
      1 => 'field_iii_iii_3_2',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-outsourcing-and-privat-gp field-group-tab ',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_outsourcing_and_privat_gp|node|country|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_political_bureaucratic|node|country|form';
  $field_group->group_name = 'group_political_bureaucratic';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_additional_indicators_gp';
  $field_group->data = array(
    'label' => 'Political, bureaucratic, and legal systems',
    'weight' => '9',
    'children' => array(
      0 => 'field_iii_v_1_1',
      1 => 'field_iii_v_1_2',
      2 => 'iii_v_1_3',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-political-bureaucratic field-group-tab ',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_political_bureaucratic|node|country|form'] = $field_group;

  return $export;
}
