<?php
/**
 * @file
 * gdp_dc.ds.inc
 */

/**
 * Implements hook_ds_custom_fields_info().
 */
function gdp_dc_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'dc';
  $ds_field->label = 'DC';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|list_centres-block',
    'block_render' => '3',
  );
  $export['dc'] = $ds_field;

  return $export;
}
