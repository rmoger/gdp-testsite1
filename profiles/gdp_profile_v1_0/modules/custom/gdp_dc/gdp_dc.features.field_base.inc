<?php
/**
 * @file
 * gdp_dc.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function gdp_dc_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_country'
  $field_bases['field_country'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_country',
    'foreign keys' => array(
      'node' => array(
        'columns' => array(
          'target_id' => 'nid',
        ),
        'table' => 'node',
      ),
    ),
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'test_field_behavior' => array(
            'status' => 0,
          ),
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'direction' => 'ASC',
          'property' => 'title',
          'type' => 'property',
        ),
        'target_bundles' => array(
          'country' => 'country',
        ),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_custodial_authority'
  $field_bases['field_custodial_authority'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_custodial_authority',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_textfield_twolevels_select',
  );

  // Exported field_base: 'field_geolocation'
  $field_bases['field_geolocation'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_geolocation',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'geofield',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'geofield',
  );

  // Exported field_base: 'field_iv_a_5_1_new'
  $field_bases['field_iv_a_5_1_new'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_iv_a_5_1_new',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'addressfield',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'addressfield',
  );

  // Exported field_base: 'field_iv_a_6_1'
  $field_bases['field_iv_a_6_1'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_iv_a_6_1',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_select',
  );

  // Exported field_base: 'field_iv_b_1_1'
  $field_bases['field_iv_b_1_1'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_iv_b_1_1',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_textfield_select',
  );

  // Exported field_base: 'field_iv_b_1_2'
  $field_bases['field_iv_b_1_2'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_iv_b_1_2',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_textfield_select',
  );

  // Exported field_base: 'field_iv_c_2_1'
  $field_bases['field_iv_c_2_1'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_iv_c_2_1',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_textfield_select',
  );

  // Exported field_base: 'field_year_ceased_operating'
  $field_bases['field_year_ceased_operating'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_year_ceased_operating',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_select',
  );

  // Exported field_base: 'iv_a_2_1'
  $field_bases['iv_a_2_1'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_a_2_1',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_select',
  );

  // Exported field_base: 'iv_a_3_1'
  $field_bases['iv_a_3_1'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_a_3_1',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_twolevels_select',
  );

  // Exported field_base: 'iv_a_3_2'
  $field_bases['iv_a_3_2'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_a_3_2',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_textfield',
  );

  // Exported field_base: 'iv_a_4_1'
  $field_bases['iv_a_4_1'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_a_4_1',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_textfield',
  );

  // Exported field_base: 'iv_a_4_2'
  $field_bases['iv_a_4_2'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_a_4_2',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_textfield',
  );

  // Exported field_base: 'iv_a_5_1'
  $field_bases['iv_a_5_1'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_a_5_1',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_textfield',
  );

  // Exported field_base: 'iv_a_8_1'
  $field_bases['iv_a_8_1'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_a_8_1',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_textfield',
  );

  // Exported field_base: 'iv_b_10_1'
  $field_bases['iv_b_10_1'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_10_1',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_select',
  );

  // Exported field_base: 'iv_b_11_1'
  $field_bases['iv_b_11_1'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_11_1',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_twolevels_select',
  );

  // Exported field_base: 'iv_b_11_2'
  $field_bases['iv_b_11_2'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_11_2',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_boolean',
  );

  // Exported field_base: 'iv_b_11_3'
  $field_bases['iv_b_11_3'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_11_3',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_boolean',
  );

  // Exported field_base: 'iv_b_12_1'
  $field_bases['iv_b_12_1'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_12_1',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_boolean',
  );

  // Exported field_base: 'iv_b_12_3'
  $field_bases['iv_b_12_3'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_12_3',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_boolean',
  );

  // Exported field_base: 'iv_b_13_1'
  $field_bases['iv_b_13_1'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_13_1',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_boolean',
  );

  // Exported field_base: 'iv_b_2_1'
  $field_bases['iv_b_2_1'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_2_1',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_select',
  );

  // Exported field_base: 'iv_b_2_2'
  $field_bases['iv_b_2_2'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_2_2',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_boolean',
  );

  // Exported field_base: 'iv_b_3_1'
  $field_bases['iv_b_3_1'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_3_1',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_select',
  );

  // Exported field_base: 'iv_b_3_2'
  $field_bases['iv_b_3_2'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_3_2',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_integer',
  );

  // Exported field_base: 'iv_b_3_3'
  $field_bases['iv_b_3_3'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_3_3',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_boolean',
  );

  // Exported field_base: 'iv_b_4_1'
  $field_bases['iv_b_4_1'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_4_1',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_select',
  );

  // Exported field_base: 'iv_b_5_1'
  $field_bases['iv_b_5_1'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_5_1',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_select',
  );

  // Exported field_base: 'iv_b_6_1'
  $field_bases['iv_b_6_1'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_6_1',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_boolean',
  );

  // Exported field_base: 'iv_b_6_2'
  $field_bases['iv_b_6_2'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_6_2',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_boolean',
  );

  // Exported field_base: 'iv_b_6_3'
  $field_bases['iv_b_6_3'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_6_3',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_boolean',
  );

  // Exported field_base: 'iv_b_6_4'
  $field_bases['iv_b_6_4'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_6_4',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_boolean',
  );

  // Exported field_base: 'iv_b_7_1'
  $field_bases['iv_b_7_1'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_7_1',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_integer',
  );

  // Exported field_base: 'iv_b_7_2'
  $field_bases['iv_b_7_2'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_7_2',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_integer',
  );

  // Exported field_base: 'iv_b_7_3'
  $field_bases['iv_b_7_3'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_7_3',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_integer',
  );

  // Exported field_base: 'iv_b_7_4'
  $field_bases['iv_b_7_4'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_7_4',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_integer',
  );

  // Exported field_base: 'iv_b_7_5'
  $field_bases['iv_b_7_5'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_7_5',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_integer',
  );

  // Exported field_base: 'iv_b_7_6'
  $field_bases['iv_b_7_6'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_7_6',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_integer',
  );

  // Exported field_base: 'iv_b_8_1'
  $field_bases['iv_b_8_1'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_8_1',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_integer',
  );

  // Exported field_base: 'iv_b_8_10'
  $field_bases['iv_b_8_10'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_8_10',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_boolean',
  );

  // Exported field_base: 'iv_b_8_2'
  $field_bases['iv_b_8_2'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_8_2',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_select',
  );

  // Exported field_base: 'iv_b_8_3'
  $field_bases['iv_b_8_3'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_8_3',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_boolean',
  );

  // Exported field_base: 'iv_b_8_4'
  $field_bases['iv_b_8_4'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_8_4',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_boolean',
  );

  // Exported field_base: 'iv_b_8_5'
  $field_bases['iv_b_8_5'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_8_5',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_boolean',
  );

  // Exported field_base: 'iv_b_8_6'
  $field_bases['iv_b_8_6'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_8_6',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_boolean',
  );

  // Exported field_base: 'iv_b_8_7'
  $field_bases['iv_b_8_7'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_8_7',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_boolean',
  );

  // Exported field_base: 'iv_b_8_8'
  $field_bases['iv_b_8_8'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_8_8',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_boolean',
  );

  // Exported field_base: 'iv_b_8_9'
  $field_bases['iv_b_8_9'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_8_9',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_boolean',
  );

  // Exported field_base: 'iv_b_9_1'
  $field_bases['iv_b_9_1'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_9_1',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_boolean',
  );

  // Exported field_base: 'iv_b_9_2'
  $field_bases['iv_b_9_2'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_b_9_2',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_boolean',
  );

  // Exported field_base: 'iv_c_3_1'
  $field_bases['iv_c_3_1'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_c_3_1',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_integer',
  );

  // Exported field_base: 'iv_c_3_2'
  $field_bases['iv_c_3_2'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_c_3_2',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_integer',
  );

  // Exported field_base: 'iv_c_4_1'
  $field_bases['iv_c_4_1'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_c_4_1',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_integer',
  );

  // Exported field_base: 'iv_c_4_2'
  $field_bases['iv_c_4_2'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_c_4_2',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_integer',
  );

  // Exported field_base: 'iv_c_5_1'
  $field_bases['iv_c_5_1'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'iv_c_5_1',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_textfield',
  );

  return $field_bases;
}
