<?php
/**
 * @file
 * gdp_dc.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function gdp_dc_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function gdp_dc_node_info() {
  $items = array(
    'dc' => array(
      'name' => t('dc'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Centre Name'),
      'help' => '',
    ),
  );
  return $items;
}
