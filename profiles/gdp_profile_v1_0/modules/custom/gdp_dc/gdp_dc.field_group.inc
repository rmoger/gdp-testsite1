<?php
/**
 * @file
 * gdp_dc.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function gdp_dc_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_a_1|node|dc|default';
  $field_group->group_name = 'group_dc_iv_a_1';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv_a_group';
  $field_group->data = array(
    'label' => 'General Information',
    'weight' => '3',
    'children' => array(
      0 => 'field_iv_a_5_1_new',
      1 => 'field_iv_a_6_1',
      2 => 'field_year_ceased_operating',
      3 => 'iv_a_2_1',
      4 => 'iv_a_3_1',
      5 => 'iv_a_3_2',
      6 => 'iv_a_4_1',
      7 => 'iv_a_4_2',
      8 => 'iv_a_8_1',
      9 => 'field_country',
      10 => 'title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'General Information',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-a-1 field-group-tab',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_dc_iv_a_1|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_a_group|node|dc|default';
  $field_group->group_name = 'group_dc_iv_a_group';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv_a';
  $field_group->data = array(
    'label' => 'dc_iv_a_group',
    'weight' => '3',
    'children' => array(
      0 => 'group_dc_iv_a_1',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-a-group field-group-tabs',
      ),
    ),
  );
  $export['group_dc_iv_a_group|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_a|node|dc|default';
  $field_group->group_name = 'group_dc_iv_a';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv';
  $field_group->data = array(
    'label' => 'General Characteristics',
    'weight' => '24',
    'children' => array(
      0 => 'group_dc_iv_a_group',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'General Characteristics',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-a field-group-htab',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_dc_iv_a|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_b_10|node|dc|default';
  $field_group->group_name = 'group_dc_iv_b_10';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv_b_group';
  $field_group->data = array(
    'label' => 'Medical care ',
    'weight' => '12',
    'children' => array(
      0 => 'iv_b_10_1',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Medical care ',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-b-10 field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_dc_iv_b_10|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_b_11|node|dc|default';
  $field_group->group_name = 'group_dc_iv_b_11';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv_b_group';
  $field_group->data = array(
    'label' => 'Access ',
    'weight' => '13',
    'children' => array(
      0 => 'iv_b_11_1',
      1 => 'iv_b_11_2',
      2 => 'iv_b_11_3',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Access ',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-b-11 field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_dc_iv_b_11|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_b_12|node|dc|default';
  $field_group->group_name = 'group_dc_iv_b_12';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv_b_group';
  $field_group->data = array(
    'label' => 'Inmate registry',
    'weight' => '14',
    'children' => array(
      0 => 'iv_b_12_1',
      1 => 'iv_b_12_3',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Inmate registry',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-b-12 field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_dc_iv_b_12|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_b_13|node|dc|default';
  $field_group->group_name = 'group_dc_iv_b_13';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv_b_group';
  $field_group->data = array(
    'label' => 'Deaths  ',
    'weight' => '15',
    'children' => array(
      0 => 'iv_b_13_1',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Deaths  ',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-b-13 field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_dc_iv_b_13|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_b_1|node|dc|default';
  $field_group->group_name = 'group_dc_iv_b_1';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv_b_group';
  $field_group->data = array(
    'label' => 'Management and services ',
    'weight' => '3',
    'children' => array(
      0 => 'field_management_1',
      1 => 'field_iv_b_1_2',
      2 => 'field_iv_b_1_1',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Management and services ',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-b-1 field-group-tab',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_dc_iv_b_1|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_b_2|node|dc|default';
  $field_group->group_name = 'group_dc_iv_b_2';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv_b_group';
  $field_group->data = array(
    'label' => 'Security ',
    'weight' => '4',
    'children' => array(
      0 => 'iv_b_2_1',
      1 => 'iv_b_2_2',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Security ',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-b-2 field-group-tab',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_dc_iv_b_2|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_b_3|node|dc|default';
  $field_group->group_name = 'group_dc_iv_b_3';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv_b_group';
  $field_group->data = array(
    'label' => 'Detention timeframe',
    'weight' => '5',
    'children' => array(
      0 => 'iv_b_3_1',
      1 => 'iv_b_3_2',
      2 => 'iv_b_3_3',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Detention timeframe',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-b-3 field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_dc_iv_b_3|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_b_4|node|dc|default';
  $field_group->group_name = 'group_dc_iv_b_4';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv_b_group';
  $field_group->data = array(
    'label' => 'Demographics',
    'weight' => '6',
    'children' => array(
      0 => 'iv_b_4_1',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Demographics',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-b-4 field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_dc_iv_b_4|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_b_5|node|dc|default';
  $field_group->group_name = 'group_dc_iv_b_5';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv_b_group';
  $field_group->data = array(
    'label' => 'Categories of detainees',
    'weight' => '7',
    'children' => array(
      0 => 'iv_b_5_1',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Categories of detainees',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-b-5 field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_dc_iv_b_5|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_b_6|node|dc|default';
  $field_group->group_name = 'group_dc_iv_b_6';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv_b_group';
  $field_group->data = array(
    'label' => 'Segregation',
    'weight' => '8',
    'children' => array(
      0 => 'iv_b_6_1',
      1 => 'iv_b_6_2',
      2 => 'iv_b_6_3',
      3 => 'iv_b_6_4',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Segregation',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-b-6 field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_dc_iv_b_6|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_b_7|node|dc|default';
  $field_group->group_name = 'group_dc_iv_b_7';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv_b_group';
  $field_group->data = array(
    'label' => 'Size and population',
    'weight' => '9',
    'children' => array(
      0 => 'iv_b_7_1',
      1 => 'iv_b_7_2',
      2 => 'iv_b_7_3',
      3 => 'iv_b_7_4',
      4 => 'iv_b_7_5',
      5 => 'iv_b_7_6',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Size and population',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-b-7 field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_dc_iv_b_7|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_b_8|node|dc|default';
  $field_group->group_name = 'group_dc_iv_b_8';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv_b_group';
  $field_group->data = array(
    'label' => 'Conditions',
    'weight' => '10',
    'children' => array(
      0 => 'iv_b_8_1',
      1 => 'iv_b_8_10',
      2 => 'iv_b_8_2',
      3 => 'iv_b_8_3',
      4 => 'iv_b_8_4',
      5 => 'iv_b_8_5',
      6 => 'iv_b_8_6',
      7 => 'iv_b_8_7',
      8 => 'iv_b_8_8',
      9 => 'iv_b_8_9',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Conditions',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-b-8 field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_dc_iv_b_8|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_b_9|node|dc|default';
  $field_group->group_name = 'group_dc_iv_b_9';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv_b_group';
  $field_group->data = array(
    'label' => 'Personnel',
    'weight' => '11',
    'children' => array(
      0 => 'iv_b_9_1',
      1 => 'iv_b_9_2',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Personnel',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-b-9 field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_dc_iv_b_9|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_b_group|node|dc|default';
  $field_group->group_name = 'group_dc_iv_b_group';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv_b';
  $field_group->data = array(
    'label' => 'dc_iv_b_group',
    'weight' => '2',
    'children' => array(
      0 => 'group_dc_iv_b_10',
      1 => 'group_dc_iv_b_11',
      2 => 'group_dc_iv_b_12',
      3 => 'group_dc_iv_b_13',
      4 => 'group_dc_iv_b_1',
      5 => 'group_dc_iv_b_2',
      6 => 'group_dc_iv_b_3',
      7 => 'group_dc_iv_b_4',
      8 => 'group_dc_iv_b_5',
      9 => 'group_dc_iv_b_6',
      10 => 'group_dc_iv_b_7',
      11 => 'group_dc_iv_b_8',
      12 => 'group_dc_iv_b_9',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-b-group field-group-tabs',
      ),
    ),
  );
  $export['group_dc_iv_b_group|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_b|node|dc|default';
  $field_group->group_name = 'group_dc_iv_b';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv';
  $field_group->data = array(
    'label' => 'Operational Characteristics',
    'weight' => '25',
    'children' => array(
      0 => 'group_dc_iv_b_group',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Operational Characteristics',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-b field-group-htab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_dc_iv_b|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_c_1|node|dc|default';
  $field_group->group_name = 'group_dc_iv_c_1';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv_c_group';
  $field_group->data = array(
    'label' => 'Custodial authority',
    'weight' => '3',
    'children' => array(
      0 => 'field_custodial_authority',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Custodial authority',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-c-1 field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_dc_iv_c_1|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_c_2|node|dc|default';
  $field_group->group_name = 'group_dc_iv_c_2';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv_c_group';
  $field_group->data = array(
    'label' => 'Ownership',
    'weight' => '4',
    'children' => array(
      0 => 'iv_c_2_1',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Ownership',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-c-2 field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_dc_iv_c_2|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_c_3|node|dc|default';
  $field_group->group_name = 'group_dc_iv_c_3';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv_c_group';
  $field_group->data = array(
    'label' => 'Budgetary and cost ',
    'weight' => '5',
    'children' => array(
      0 => 'iv_c_3_1',
      1 => 'iv_c_3_2',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Budgetary and cost ',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-c-3 field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_dc_iv_c_3|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_c_4|node|dc|default';
  $field_group->group_name = 'group_dc_iv_c_4';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv_c_group';
  $field_group->data = array(
    'label' => 'External funding and building contractors ',
    'weight' => '6',
    'children' => array(
      0 => 'iv_c_4_1',
      1 => 'iv_c_4_2',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'External funding and building contractors ',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-c-4 field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_dc_iv_c_4|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_c_5|node|dc|default';
  $field_group->group_name = 'group_dc_iv_c_5';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv_c_group';
  $field_group->data = array(
    'label' => 'Area of authority ',
    'weight' => '7',
    'children' => array(
      0 => 'iv_c_5_1',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Area of authority ',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-c-5 field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_dc_iv_c_5|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_c_group|node|dc|default';
  $field_group->group_name = 'group_dc_iv_c_group';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv_c';
  $field_group->data = array(
    'label' => 'dc_iv_c_group',
    'weight' => '3',
    'children' => array(
      0 => 'group_dc_iv_c_1',
      1 => 'group_dc_iv_c_2',
      2 => 'group_dc_iv_c_3',
      3 => 'group_dc_iv_c_4',
      4 => 'group_dc_iv_c_5',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-c-group field-group-tabs',
      ),
    ),
  );
  $export['group_dc_iv_c_group|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv_c|node|dc|default';
  $field_group->group_name = 'group_dc_iv_c';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv';
  $field_group->data = array(
    'label' => 'Bureaucratic Characteristics',
    'weight' => '26',
    'children' => array(
      0 => 'group_dc_iv_c_group',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Bureaucratic Characteristics',
      'instance_settings' => array(
        'classes' => ' group-dc-iv-c field-group-htab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_dc_iv_c|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dc_iv|node|dc|default';
  $field_group->group_name = 'group_dc_iv';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'General Characteristics',
    'weight' => '0',
    'children' => array(
      0 => 'group_dc_iv_a',
      1 => 'group_dc_iv_b',
      2 => 'group_dc_iv_c',
      3 => 'group_location',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'label' => 'General Characteristics',
      'instance_settings' => array(
        'classes' => ' group-dc-iv field-group-htabs',
      ),
    ),
  );
  $export['group_dc_iv|node|dc|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_location|node|dc|default';
  $field_group->group_name = 'group_location';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dc';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_dc_iv';
  $field_group->data = array(
    'label' => 'Location',
    'weight' => '27',
    'children' => array(
      0 => 'field_geolocation',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => ' group-location field-group-htab',
      ),
    ),
  );
  $export['group_location|node|dc|default'] = $field_group;

  return $export;
}
