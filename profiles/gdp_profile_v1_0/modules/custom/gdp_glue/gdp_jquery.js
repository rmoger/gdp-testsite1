(function ($) {
  Drupal.behaviors.gdpjquery =  {
    attach: function(context, settings) {

    //$("strong:contains('Treaties')").closest('li').parent().html("<a href='#' id='treaties_anchor'> <strong>Treaties<strong></a>");


    $("strong:contains('Legal Framework')").click(function() {
    
    	//alert($(this).html());

	    if (typeof(Drupal.settings.gdp_glue.treaty_path) != "undefined") {
	    	window.location = Drupal.settings.gdp_glue.treaty_path;
	    	return false;
	    }
	    	
    });

    $("strong:contains('Legal Framework'),  strong:contains('Immigration and Detention Statistics'), strong:contains('Socio-Economic Indicators')").click(function() {
    
	    if (typeof(Drupal.settings.gdp_glue.country_path) != "undefined") {
	    	
	    	window.location = Drupal.settings.gdp_glue.country_path;
	    	return false;
	    }
	    	
    });

    $("strong:contains('Institutional Indicators')").click(function() {
    
	    if (typeof(Drupal.settings.gdp_glue.inst_ind_path) != "undefined") {
	    	
	    	window.location = Drupal.settings.gdp_glue.inst_ind_path;
	    	return false;
	    }
	    	
    });


    $("strong:contains('Immigration and Detention Statistics')").click(function() {
    
	    if (typeof(Drupal.settings.gdp_glue.immig_path_path) != "undefined") {
	    	
	    	window.location = Drupal.settings.gdp_glue.immig_path_path;
	    	return false;
	    }
	    	
    });



$('html, body').animate({ scrollTop: 0 }, 'fast');




    }
  };
})(jQuery);