<?php
/**
 ** Test migrate
 */


class gdp_basic_migration extends Migration {
  /**
    * Class constructor, the one that initiates the process
    */
  public function __construct() {
    
   parent::__construct();    

    $this->description = 'Imports name type and status';

      // $fields = array(
      //     'importid',
      //     'name',
      //     'type',
      //     'status',
      //     'source'
      // );

     $query = db_select('nametypestatus_import', 'n')
               ->fields('n');


    $this->source = new MigrateSourceSQL($query);

    //dpm($this->desination);

    $this->destination = new MigrateDestinationNode('namestypestatus');

    $pkmap = array('importid' => array('type'  => 'int',
                                     'unsigned'  => TRUE,
                                     'not null'  => TRUE
                                     ));

    $this->map = new MigrateSQLMap($this->machineName, $pkmap, MigrateDestinationNode::getKeySchema());


    // $this->addSimpleMappings(array('title'));

    $this->addFieldMapping('title', 'name');
    $this->addFieldMapping('field_type', 'type');
 //   $this->addFieldMapping('field_gdp_status', 'status');
    $this->addFieldMapping('field_gdp_status_select', 'status');
    $this->addFieldMapping('field_gdp_status_select:value', 'status');
    $this->addFieldMapping('field_gdp_status_select:gdpsource1', 'source');
    $this->addFieldMapping('field_gdp_status_select:gdpsource2', 'source');
    $this->addFieldMapping('field_gdp_status_select:gdpsource3', 'source');

  

    // $this->addFieldMapping('field_gdp_status_select:gdpsource1', 'source');
    // $this->addFieldMapping('field_gdp_status_select:gdpsource2', 'source');
    // $this->addFieldMapping('field_gdp_status_select:gdpsource3', 'source');
    // $this->addFieldMapping('field_gdp_longtext', 'source');

  }



    public function prepareRow($row) {
      
      //$row->source1 = $source1;
      // $row->field_gdp_status = $status;
      // $row->field_gdp_status = $status;
      //dpm($row);
      //dpm($this);

      /* $sources = explode(',', $row->source);
      foreach ($sources as $key => $source) {
        $row->{'source' . $key} = array('value' => $source, 'format' => 'plain_text');
      }
      */

      $row->source = array('value' => $row->source, 'format' => 'plain_text');


    }

    public function prepare($entity) {
     dpm($entity);
    }


}

/**
 * Base class for creating field handlers for fields with a single value.
 *
 * To use this class just extend it and pass key where the field's value should
 * be stored to the constructor, then register the type(s):
 * @code
 *   class MigrateLinkFieldHandler extends MigrateSimpleFieldHandler {
 *     public function __construct() {
 *       parent::__construct('url');
 *       $this->registerTypes(array('link'));
 *     }
 *   }
 * @endcode
 */
  class MigrateGDPHandler extends MigrateValueFieldHandler {
    // protected $fieldValueKey = 'value';

    // protected $skipEmpty = FALSE;

    /**
     * Construct a simple field handler.
     *
     * @param $options
     *   Array of options (rather than unamed parameters so you don't have to
     *   what TRUE or FALSE means). The following keys are used:
     *   - 'value_key' string with the name of the key in the fields value array.
     *   - 'skip_empty' Boolean indicating that empty values should not be saved.
     */


    public function __construct($options = array()) {
      $this->registerTypes(array('gdp_textfield', 'gdp_select', 'gdp_status_select','field_gdp_status_select')); 
    }


/**
   * Implementation of MigrateFieldHandler::fields().
   *
   * @param $type
   *  The file field type - 'file' or 'image'
   * @param $parent_field
   *  Name of the parent field.
   * @param Migration $migration
   *  The migration context for the parent field. We can look at the mappings
   *  and determine which subfields are relevant.
   * @return array
   */
  public function fields($type, $parent_field, $migration = NULL) {
 
    $fields = array(
      'ratification_date' => t('Subfield: String to be used as the name value'),
      'observation_date' => t('Subfield: String to be used as the street value'),
      'gdpsource1' => t('Subfield: String to be used as the additional value'),
      'gdpsource2' => '',
      'gdpsource3' => t('Subfield: String to be used as the province value'),
      'status' => t('Subfield: String to be used as the postal code value'),
      'weight' => t('Subfield: Float to be used as the latitude value'),
      'value' => t('Subfield: Float to be used as the longitude value'),
      'ref' => t('Subfield: String (ISO code) to be used as the country value'),
    );
    return $fields;
  }




    public function prepare($entity, array $field_info, array $instance, array $values) {

      $arguments = array();
      if (isset($values['arguments'])) {
        $arguments = array_filter($values['arguments']);
        unset($values['arguments']);
      }

      $language = $this->getFieldLanguage($entity, $field_info, $arguments);
      //dpm($arguments);
      //dpm($field_info);
      // Setup the standard Field API array for saving.
      $delta = 0;
      foreach ($values as $value) {
        //dpm($arguments);
        $return[$language][$delta] = array_intersect_key($arguments, $field_info['columns']);
        $delta++;
      }
      //dpm($return);
      return isset($return) ? $return : NULL;
    }


    /**
     * Returns TRUE only for values which are not NULL.
     *
     * @param $value
     * @return bool
     */
    protected function notNull($value) {
      return !is_null($value);
    }
  }






///---------------------------

      // if (isset($arguments['gdpsource1'])) {
      //   if (is_array($arguments['gdpsource1'])) {
      //     $return[$language][$delta]['extension'] = $arguments['gdpsource1'][$delta];
      //   }
      //   else {
      //     $return[$language][$delta]['gdpsource1'] = $arguments['gdpsource1'];
      //   }
      // }

      // dpm($arguments, 'arguments');
      // dpm($return, 'return');

///---------------------------
