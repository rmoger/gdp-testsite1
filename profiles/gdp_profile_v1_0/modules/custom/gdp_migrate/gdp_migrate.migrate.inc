 <?php


  function gdp_migrate_migrate_api() {

    $api = array(
      'api' => 2,

      'migrations' => array(
        'gdp_basic_migration' => array('class_name' => 'gdp_basic_migration'),
        ),

      'field handlers' => array(
        'MigrateGDPHandler'
        ),

    );

    //dsm('Registering GDP Migration classes');

    return $api;
  }