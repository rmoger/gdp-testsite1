<?php
/**
 * @file
 * treaty.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function treaty_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function treaty_node_info() {
  $items = array(
    'treaty' => array(
      'name' => t('Treaty'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Treaty Name'),
      'help' => '',
    ),
  );
  return $items;
}
