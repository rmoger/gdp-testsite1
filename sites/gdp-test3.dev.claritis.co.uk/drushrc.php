<?php 


$options['db_type'] = 'mysql';
$options['db_host'] = 'localhost';
$options['db_port'] = '3306';
$options['db_passwd'] = 'Ysuu7yMovs';
$options['db_name'] = 'gdptest3devcla_0';
$options['db_user'] = 'gdptest3devcla_0';
$options['packages'] = array (
  'platforms' => 
  array (
    'drupal' => 
    array (
      'short_name' => 'drupal',
      'version' => '7.22',
      'description' => 'This platform is running Drupal 7.22',
    ),
  ),
  'profiles' => 
  array (
    'gdp_profile_v1_0' => 
    array (
      'name' => 'gdp_profile_v1_0',
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/gdp_profile_v1_0.profile',
      'project' => '',
      'info' => 
      array (
        'name' => 'GDP Profile V1.0',
        'description' => '',
        'exclusive' => '1',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'admin',
          1 => 'admin_devel',
          2 => 'admin_menu',
          3 => 'admin_menu_toolbar',
          4 => 'ctools',
          5 => 'block',
          6 => 'color',
          7 => 'comment',
          8 => 'contextual',
          9 => 'dashboard',
          10 => 'dblog',
          11 => 'field',
          12 => 'field_sql_storage',
          13 => 'field_ui',
          14 => 'file',
          15 => 'filter',
          16 => 'help',
          17 => 'image',
          18 => 'list',
          19 => 'menu',
          20 => 'node',
          21 => 'number',
          22 => 'options',
          23 => 'overlay',
          24 => 'path',
          25 => 'rdf',
          26 => 'search',
          27 => 'shortcut',
          28 => 'system',
          29 => 'taxonomy',
          30 => 'text',
          31 => 'update',
          32 => 'user',
          33 => 'date',
          34 => 'date_api',
          35 => 'ds',
          36 => 'argfilters',
          37 => 'entityreference',
          38 => 'entityreference_behavior_example',
          39 => 'field_group',
          40 => 'link',
          41 => 'better_formats',
          42 => 'diff',
          43 => 'entity',
          44 => 'entity_token',
          45 => 'libraries',
          46 => 'module_filter',
          47 => 'rel',
          48 => 'select_or_other',
          49 => 'strongarm',
          50 => 'token',
          51 => 'views',
          52 => 'views_ui',
          53 => 'gdp_fields',
          54 => 'features',
          55 => 'country',
          56 => 'detention_centre',
          57 => 'sub_region',
          58 => 'treaty',
        ),
        'version' => NULL,
        'php' => '5.2.4',
        'languages' => 
        array (
          0 => 'en',
        ),
      ),
      'version' => '7.22',
      'status' => 1,
    ),
  ),
  'modules' => 
  array (
    'aaa_update_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/update/tests/aaa_update_test.module',
      'name' => 'aaa_update_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'AAA Update test',
        'description' => 'Support module for update module testing.',
        'package' => 'Testing',
        'core' => '7.x',
        'hidden' => true,
        'version' => '7.22',
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'actions_loop_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/actions_loop_test.module',
      'name' => 'actions_loop_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Actions loop test',
        'description' => 'Support module for action loop testing.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'actions_permissions' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/views_bulk_operations/actions_permissions.module',
      'name' => 'actions_permissions',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Actions permissions (VBO)',
        'description' => 'Provides permission-based access control for actions. Used by Views Bulk Operations.',
        'package' => 'Administration',
        'core' => '7.x',
        'version' => '7.x-3.1',
        'project' => 'views_bulk_operations',
        'datestamp' => '1354500015',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'views_bulk_operations',
      'version' => '7.x-3.1',
    ),
    'addressfield' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/addressfield/addressfield.module',
      'name' => 'addressfield',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7000',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Address Field',
        'description' => 'Manage a flexible address field, implementing the xNAL standard.',
        'core' => '7.x',
        'package' => 'Fields',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'files' => 
        array (
          0 => 'views/addressfield_views_handler_filter_country.inc',
        ),
        'version' => '7.x-1.0-beta3',
        'project' => 'addressfield',
        'datestamp' => '1338304248',
        'php' => '5.2.4',
      ),
      'project' => 'addressfield',
      'version' => '7.x-1.0-beta3',
    ),
    'addressfield_example' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/addressfield/example/addressfield_example.module',
      'name' => 'addressfield_example',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Address Field Example',
        'description' => 'Example module for how to implement an addressfield format handler.',
        'core' => '7.x',
        'package' => 'Fields',
        'hidden' => true,
        'dependencies' => 
        array (
          0 => 'ctools',
          1 => 'addressfield',
        ),
        'version' => '7.x-1.0-beta3',
        'project' => 'addressfield',
        'datestamp' => '1338304248',
        'php' => '5.2.4',
      ),
      'project' => 'addressfield',
      'version' => '7.x-1.0-beta3',
    ),
    'admin' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/admin/admin.module',
      'name' => 'admin',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '1',
      'info' => 
      array (
        'name' => 'Admin',
        'description' => 'UI helpers for Drupal admins and managers.',
        'package' => 'Administration',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'admin.admin.inc',
          1 => 'admin.install',
          2 => 'admin.module',
          3 => 'includes/admin.devel.inc',
          4 => 'includes/admin.theme.inc',
          5 => 'theme/admin-panes.tpl.php',
          6 => 'theme/admin-toolbar.tpl.php',
          7 => 'theme/theme.inc',
        ),
        'version' => '7.x-2.0-beta3',
        'project' => 'admin',
        'datestamp' => '1292541646',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'admin',
      'version' => '7.x-2.0-beta3',
    ),
    'admin_devel' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/admin_menu/admin_devel/admin_devel.module',
      'name' => 'admin_devel',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Administration Development tools',
        'description' => 'Administration and debugging functionality for developers and site builders.',
        'package' => 'Administration',
        'core' => '7.x',
        'scripts' => 
        array (
          0 => 'admin_devel.js',
        ),
        'version' => '7.x-3.0-rc4',
        'project' => 'admin_menu',
        'datestamp' => '1359651687',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'admin_menu',
      'version' => '7.x-3.0-rc4',
    ),
    'admin_menu' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/admin_menu/admin_menu.module',
      'name' => 'admin_menu',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7304',
      'weight' => '100',
      'info' => 
      array (
        'name' => 'Administration menu',
        'description' => 'Provides a dropdown menu to most administrative tasks and other common destinations (to users with the proper permissions).',
        'package' => 'Administration',
        'core' => '7.x',
        'configure' => 'admin/config/administration/admin_menu',
        'dependencies' => 
        array (
          0 => 'system (>7.10)',
        ),
        'files' => 
        array (
          0 => 'tests/admin_menu.test',
        ),
        'version' => '7.x-3.0-rc4',
        'project' => 'admin_menu',
        'datestamp' => '1359651687',
        'php' => '5.2.4',
      ),
      'project' => 'admin_menu',
      'version' => '7.x-3.0-rc4',
    ),
    'admin_menu_toolbar' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/admin_menu/admin_menu_toolbar/admin_menu_toolbar.module',
      'name' => 'admin_menu_toolbar',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '6300',
      'weight' => '101',
      'info' => 
      array (
        'name' => 'Administration menu Toolbar style',
        'description' => 'A better Toolbar.',
        'package' => 'Administration',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'admin_menu',
        ),
        'version' => '7.x-3.0-rc4',
        'project' => 'admin_menu',
        'datestamp' => '1359651687',
        'php' => '5.2.4',
      ),
      'project' => 'admin_menu',
      'version' => '7.x-3.0-rc4',
    ),
    'aggregator' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/aggregator/aggregator.module',
      'name' => 'aggregator',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Aggregator',
        'description' => 'Aggregates syndicated content (RSS, RDF, and Atom feeds).',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'aggregator.test',
        ),
        'configure' => 'admin/config/services/aggregator/settings',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'aggregator.css',
          ),
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'aggregator_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/aggregator/tests/aggregator_test.module',
      'name' => 'aggregator_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Aggregator module tests',
        'description' => 'Support module for aggregator related testing.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'ajax_forms_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/ajax_forms_test.module',
      'name' => 'ajax_forms_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'AJAX form test mock module',
        'description' => 'Test for AJAX form calls.',
        'core' => '7.x',
        'package' => 'Testing',
        'version' => '7.22',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'ajax_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/ajax_test.module',
      'name' => 'ajax_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'AJAX Test',
        'description' => 'Support module for AJAX framework tests.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'argfilters' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/views_arguments_in_filters/argfilters.module',
      'name' => 'argfilters',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views arguments in filters',
        'description' => 'Allows you to use argument values in filters, thereby leveraging functionality such as operators.',
        'package' => 'Experimental',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'views',
        ),
        'version' => NULL,
        'php' => '5.2.4',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'auto_nodetitle' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/auto_nodetitle/auto_nodetitle.module',
      'name' => 'auto_nodetitle',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '1',
      'weight' => '5',
      'info' => 
      array (
        'name' => 'Automatic Nodetitles',
        'description' => 'Allows hiding of the content title field and automatic title creation.',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'auto_nodetitle.install',
          1 => 'auto_nodetitle.module',
          2 => 'auto_nodetitle.js',
        ),
        'version' => '7.x-1.0',
        'project' => 'auto_nodetitle',
        'datestamp' => '1307449915',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'auto_nodetitle',
      'version' => '7.x-1.0',
    ),
    'batch_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/batch_test.module',
      'name' => 'batch_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Batch API test',
        'description' => 'Support module for Batch API tests.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'bbb_update_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/update/tests/bbb_update_test.module',
      'name' => 'bbb_update_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'BBB Update test',
        'description' => 'Support module for update module testing.',
        'package' => 'Testing',
        'core' => '7.x',
        'hidden' => true,
        'version' => '7.22',
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'better_formats' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/better_formats/better_formats.module',
      'name' => 'better_formats',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '100',
      'info' => 
      array (
        'name' => 'Better Formats',
        'description' => 'Enhances the core input format system by managing input format defaults and settings.',
        'core' => '7.x',
        'configure' => 'admin/config/content/formats',
        'version' => '7.x-1.0-beta1',
        'project' => 'better_formats',
        'datestamp' => '1343262404',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'better_formats',
      'version' => '7.x-1.0-beta1',
    ),
    'block' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/block/block.module',
      'name' => 'block',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7008',
      'weight' => '-5',
      'info' => 
      array (
        'name' => 'Block',
        'description' => 'Controls the visual building blocks a page is constructed with. Blocks are boxes of content rendered into an area, or region, of a web page.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'block.test',
        ),
        'configure' => 'admin/structure/block',
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'block_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/block/tests/block_test.module',
      'name' => 'block_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Block test',
        'description' => 'Provides test blocks.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'blog' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/blog/blog.module',
      'name' => 'blog',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Blog',
        'description' => 'Enables multi-user blogs.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'blog.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'book' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/book/book.module',
      'name' => 'book',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Book',
        'description' => 'Allows users to create and organize related content in an outline.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'book.test',
        ),
        'configure' => 'admin/content/book/settings',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'book.css',
          ),
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'bulk_export' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/ctools/bulk_export/bulk_export.module',
      'name' => 'bulk_export',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Bulk Export',
        'description' => 'Performs bulk exporting of data objects known about by Chaos tools.',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'package' => 'Chaos tool suite',
        'version' => '7.x-1.3',
        'project' => 'ctools',
        'datestamp' => '1365013512',
        'php' => '5.2.4',
      ),
      'project' => 'ctools',
      'version' => '7.x-1.3',
    ),
    'bundle_copy' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/bundle_copy/bundle_copy.module',
      'name' => 'bundle_copy',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Bundle copy',
        'description' => 'Import and exports bundles through the UI.',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'package' => 'Fields',
        'files' => 
        array (
          0 => 'bundle_copy.module',
        ),
        'version' => '7.x-1.1',
        'project' => 'bundle_copy',
        'datestamp' => '1332926440',
        'php' => '5.2.4',
      ),
      'project' => 'bundle_copy',
      'version' => '7.x-1.1',
    ),
    'ccc_update_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/update/tests/ccc_update_test.module',
      'name' => 'ccc_update_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'CCC Update test',
        'description' => 'Support module for update module testing.',
        'package' => 'Testing',
        'core' => '7.x',
        'hidden' => true,
        'version' => '7.22',
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'ckeditor' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/ckeditor/ckeditor.module',
      'name' => 'ckeditor',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'CKEditor',
        'description' => 'Enables CKEditor (WYSIWYG HTML editor) for use instead of plain text fields.',
        'core' => '7.x',
        'package' => 'User interface',
        'configure' => 'admin/config/content/ckeditor',
        'version' => '7.x-1.13',
        'project' => 'ckeditor',
        'datestamp' => '1365759619',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'ckeditor',
      'version' => '7.x-1.13',
    ),
    'color' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/color/color.module',
      'name' => 'color',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7001',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Color',
        'description' => 'Allows administrators to change the color scheme of compatible themes.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'color.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'comment' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/comment/comment.module',
      'name' => 'comment',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7009',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Comment',
        'description' => 'Allows users to comment on and discuss published content.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'text',
        ),
        'files' => 
        array (
          0 => 'comment.module',
          1 => 'comment.test',
        ),
        'configure' => 'admin/content/comment',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'comment.css',
          ),
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'common_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/common_test.module',
      'name' => 'common_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Common Test',
        'description' => 'Support module for Common tests.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'common_test.css',
          ),
          'print' => 
          array (
            0 => 'common_test.print.css',
          ),
        ),
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'common_test_cron_helper' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/common_test_cron_helper.module',
      'name' => 'common_test_cron_helper',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Common Test Cron Helper',
        'description' => 'Helper module for CronRunTestCase::testCronExceptions().',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'contact' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/contact/contact.module',
      'name' => 'contact',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Contact',
        'description' => 'Enables the use of both personal and site-wide contact forms.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'contact.test',
        ),
        'configure' => 'admin/structure/contact',
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'context' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/context/context.module',
      'name' => 'context',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7000',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Context',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'description' => 'Provide modules with a cache that lasts for a single page request.',
        'package' => 'Context',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'context.module',
          1 => 'tests/context.test',
          2 => 'tests/context.conditions.test',
          3 => 'tests/context.reactions.test',
        ),
        'version' => '7.x-3.0-beta6',
        'project' => 'context',
        'datestamp' => '1355879811',
        'php' => '5.2.4',
      ),
      'project' => 'context',
      'version' => '7.x-3.0-beta6',
    ),
    'contextual' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/contextual/contextual.module',
      'name' => 'contextual',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Contextual links',
        'description' => 'Provides contextual links to perform actions related to elements on a page.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'contextual.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'context_layouts' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/context/context_layouts/context_layouts.module',
      'name' => 'context_layouts',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Context layouts',
        'description' => 'Allow theme layer to provide multiple region layouts and integrate with context.',
        'dependencies' => 
        array (
          0 => 'context',
        ),
        'package' => 'Context',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'plugins/context_layouts_reaction_block.inc',
        ),
        'version' => '7.x-3.0-beta6',
        'project' => 'context',
        'datestamp' => '1355879811',
        'php' => '5.2.4',
      ),
      'project' => 'context',
      'version' => '7.x-3.0-beta6',
    ),
    'context_ui' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/context/context_ui/context_ui.module',
      'name' => 'context_ui',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '6004',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Context UI',
        'description' => 'Provides a simple UI for settings up a site structure using Context.',
        'dependencies' => 
        array (
          0 => 'context',
        ),
        'package' => 'Context',
        'core' => '7.x',
        'configure' => 'admin/structure/context',
        'files' => 
        array (
          0 => 'context.module',
          1 => 'tests/context_ui.test',
        ),
        'version' => '7.x-3.0-beta6',
        'project' => 'context',
        'datestamp' => '1355879811',
        'php' => '5.2.4',
      ),
      'project' => 'context',
      'version' => '7.x-3.0-beta6',
    ),
    'country' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22',
      'name' => 'country',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '5.2.4',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'country_edit_form' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/custom/country_edit_form/country_edit_form.module',
      'name' => 'country_edit_form',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'country edit form',
        'core' => '7.x',
        'package' => 'Features',
        'php' => '5.2.4',
        'dependencies' => 
        array (
          0 => 'ctools',
          1 => 'ds',
          2 => 'field_group',
        ),
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'ds:ds:1',
            1 => 'field_group:field_group:1',
          ),
          'ds_layout_settings' => 
          array (
            0 => 'node|country|form',
          ),
          'features_api' => 
          array (
            0 => 'api:1',
          ),
          'field_group' => 
          array (
            0 => 'group_additional_indicators_gp|node|country|form',
            1 => 'group_additional_indicators|node|country|form',
            2 => 'group_agreements|node|country|form',
            3 => 'group_attitudes_perceptions_gp|node|country|form',
            4 => 'group_country_tabs|node|country|form',
            5 => 'group_detention_centres|node|country|form',
            6 => 'group_eco_and_development_gp|node|country|form',
            7 => 'group_iii_iii_1|node|country|form',
            8 => 'group_iii_iii_2|node|country|form',
            9 => 'group_iii_iv_1|node|country|form',
            10 => 'group_iii_iv_2|node|country|form',
            11 => 'group_immigration_indicators_gp|node|country|form',
            12 => 'group_immigration_indicators|node|country|form',
            13 => 'group_instit_indicators_grp|node|country|form',
            14 => 'group_institutional_indicators|node|country|form',
            15 => 'group_legal_framework|node|country|form',
            16 => 'group_non_state_funding_gp|node|country|form',
            17 => 'group_outsourcing_and_privat_gp|node|country|form',
            18 => 'group_political_bureaucratic|node|country|form',
          ),
        ),
        'project path' => 'profiles/gdp_profile_v1_0/modules/custom',
        'description' => '',
        'version' => NULL,
      ),
      'project' => '',
      'version' => NULL,
    ),
    'country_node_type' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/custom/country_node_type/country_node_type.module',
      'name' => 'country_node_type',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'country node type',
        'core' => '7.x',
        'package' => 'Features',
        'php' => '5.2.4',
        'dependencies' => 
        array (
          0 => 'ctools',
          1 => 'features',
          2 => 'field_group',
          3 => 'gdp_fields',
          4 => 'strongarm',
          5 => 'text',
        ),
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'strongarm:strongarm:1',
          ),
          'features_api' => 
          array (
            0 => 'api:1',
          ),
          'field_base' => 
          array (
            0 => 'field_apprehending_authorities',
            1 => 'field_dummy_field_2',
            2 => 'field_dummy_field_5',
            3 => 'field_iii_iii_1_1',
            4 => 'field_iii_iii_1_4x',
            5 => 'field_iii_v_1_1',
            6 => 'field_iii_v_1_2',
            7 => 'iii_iii_1_3',
            8 => 'iii_iii_2_1',
            9 => 'iii_iii_2_10',
            10 => 'iii_iii_2_11',
            11 => 'iii_iii_2_12',
            12 => 'iii_iii_2_13',
            13 => 'iii_iii_2_14',
            14 => 'iii_iii_2_15',
            15 => 'iii_iii_2_16',
            16 => 'iii_iii_2_17',
            17 => 'iii_iii_2_18',
            18 => 'iii_iii_2_19',
            19 => 'iii_iii_2_2',
            20 => 'iii_iii_2_3',
            21 => 'iii_iii_2_4',
            22 => 'iii_iii_2_5',
            23 => 'iii_iii_2_6',
            24 => 'iii_iii_2_7',
            25 => 'iii_iii_2_8',
            26 => 'iii_iii_2_9',
            27 => 'iii_iii_3_1',
            28 => 'iii_iii_4_1',
            29 => 'iii_iii_4_2',
            30 => 'iii_iv_1_1',
            31 => 'iii_iv_1_10',
            32 => 'iii_iv_1_11',
            33 => 'iii_iv_1_12',
            34 => 'iii_iv_1_13',
            35 => 'iii_iv_1_14',
            36 => 'iii_iv_1_15',
            37 => 'iii_iv_1_16',
            38 => 'iii_iv_1_17',
            39 => 'iii_iv_1_2',
            40 => 'iii_iv_1_3',
            41 => 'iii_iv_1_4',
            42 => 'iii_iv_1_5',
            43 => 'iii_iv_1_6',
            44 => 'iii_iv_1_7',
            45 => 'iii_iv_1_8',
            46 => 'iii_iv_1_9',
            47 => 'iii_iv_2_1',
            48 => 'iii_iv_2_2',
            49 => 'iii_iv_2_3',
            50 => 'iii_iv_2_3_1',
            51 => 'iii_iv_2_4',
            52 => 'iii_iv_2_5',
            53 => 'iii_iv_2_6',
            54 => 'iii_iv_2_7',
            55 => 'iii_iv_2_8',
            56 => 'iii_iv_2_9',
            57 => 'iii_v_1_3',
            58 => 'iii_v_2_1',
            59 => 'iii_v_2_2',
            60 => 'iii_v_2_3',
            61 => 'iii_v_2_4',
            62 => 'iii_v_2_5',
            63 => 'iii_v_2_6',
            64 => 'iii_v_2_7',
            65 => 'iii_v_3_1',
            66 => 'iii_v_3_2',
            67 => 'iii_v_3_3',
            68 => 'iii_v_3_4',
          ),
          'field_instance' => 
          array (
            0 => 'node-country-body',
            1 => 'node-country-field_apprehending_authorities',
            2 => 'node-country-field_dummy_field_2',
            3 => 'node-country-field_dummy_field_5',
            4 => 'node-country-field_iii_iii_1_1',
            5 => 'node-country-field_iii_iii_1_4x',
            6 => 'node-country-field_iii_v_1_1',
            7 => 'node-country-field_iii_v_1_2',
            8 => 'node-country-iii_iii_1_3',
            9 => 'node-country-iii_iii_2_1',
            10 => 'node-country-iii_iii_2_10',
            11 => 'node-country-iii_iii_2_11',
            12 => 'node-country-iii_iii_2_12',
            13 => 'node-country-iii_iii_2_13',
            14 => 'node-country-iii_iii_2_14',
            15 => 'node-country-iii_iii_2_15',
            16 => 'node-country-iii_iii_2_16',
            17 => 'node-country-iii_iii_2_17',
            18 => 'node-country-iii_iii_2_18',
            19 => 'node-country-iii_iii_2_19',
            20 => 'node-country-iii_iii_2_2',
            21 => 'node-country-iii_iii_2_3',
            22 => 'node-country-iii_iii_2_4',
            23 => 'node-country-iii_iii_2_5',
            24 => 'node-country-iii_iii_2_6',
            25 => 'node-country-iii_iii_2_7',
            26 => 'node-country-iii_iii_2_8',
            27 => 'node-country-iii_iii_2_9',
            28 => 'node-country-iii_iii_3_1',
            29 => 'node-country-iii_iii_4_1',
            30 => 'node-country-iii_iii_4_2',
            31 => 'node-country-iii_iv_1_1',
            32 => 'node-country-iii_iv_1_10',
            33 => 'node-country-iii_iv_1_11',
            34 => 'node-country-iii_iv_1_12',
            35 => 'node-country-iii_iv_1_13',
            36 => 'node-country-iii_iv_1_14',
            37 => 'node-country-iii_iv_1_15',
            38 => 'node-country-iii_iv_1_16',
            39 => 'node-country-iii_iv_1_17',
            40 => 'node-country-iii_iv_1_2',
            41 => 'node-country-iii_iv_1_3',
            42 => 'node-country-iii_iv_1_4',
            43 => 'node-country-iii_iv_1_5',
            44 => 'node-country-iii_iv_1_6',
            45 => 'node-country-iii_iv_1_7',
            46 => 'node-country-iii_iv_1_8',
            47 => 'node-country-iii_iv_1_9',
            48 => 'node-country-iii_iv_2_1',
            49 => 'node-country-iii_iv_2_2',
            50 => 'node-country-iii_iv_2_3',
            51 => 'node-country-iii_iv_2_3_1',
            52 => 'node-country-iii_iv_2_4',
            53 => 'node-country-iii_iv_2_5',
            54 => 'node-country-iii_iv_2_6',
            55 => 'node-country-iii_iv_2_7',
            56 => 'node-country-iii_iv_2_8',
            57 => 'node-country-iii_iv_2_9',
            58 => 'node-country-iii_v_1_3',
            59 => 'node-country-iii_v_2_1',
            60 => 'node-country-iii_v_2_2',
            61 => 'node-country-iii_v_2_3',
            62 => 'node-country-iii_v_2_4',
            63 => 'node-country-iii_v_2_5',
            64 => 'node-country-iii_v_2_6',
            65 => 'node-country-iii_v_2_7',
            66 => 'node-country-iii_v_3_1',
            67 => 'node-country-iii_v_3_2',
            68 => 'node-country-iii_v_3_3',
            69 => 'node-country-iii_v_3_4',
          ),
          'node' => 
          array (
            0 => 'country',
          ),
          'variable' => 
          array (
            0 => 'comment_anonymous_country',
            1 => 'comment_country',
            2 => 'comment_default_mode_country',
            3 => 'comment_default_per_page_country',
            4 => 'comment_form_location_country',
            5 => 'comment_preview_country',
            6 => 'comment_subject_field_country',
            7 => 'field_bundle_settings_node__country',
            8 => 'menu_options_country',
            9 => 'menu_parent_country',
            10 => 'node_options_country',
            11 => 'node_preview_country',
            12 => 'node_submitted_country',
          ),
        ),
        'features_exclude' => 
        array (
          'field' => 
          array (
            'node-country-body' => 'node-country-body',
            'node-country-field_dummy_field_2' => 'node-country-field_dummy_field_2',
            'node-country-field_dummy_field_5' => 'node-country-field_dummy_field_5',
            'node-country-field_iii_v_1_1' => 'node-country-field_iii_v_1_1',
            'node-country-field_iii_v_1_2' => 'node-country-field_iii_v_1_2',
            'node-country-iii_v_3_2' => 'node-country-iii_v_3_2',
            'node-country-iii_v_3_3' => 'node-country-iii_v_3_3',
            'node-country-iii_v_3_4' => 'node-country-iii_v_3_4',
            'node-country-iii_v_1_3' => 'node-country-iii_v_1_3',
            'node-country-iii_v_2_1' => 'node-country-iii_v_2_1',
            'node-country-iii_v_2_2' => 'node-country-iii_v_2_2',
            'node-country-iii_v_2_3' => 'node-country-iii_v_2_3',
            'node-country-iii_v_2_4' => 'node-country-iii_v_2_4',
            'node-country-iii_v_2_5' => 'node-country-iii_v_2_5',
            'node-country-iii_v_2_6' => 'node-country-iii_v_2_6',
            'node-country-iii_v_2_7' => 'node-country-iii_v_2_7',
            'node-country-iii_v_3_1' => 'node-country-iii_v_3_1',
            'node-country-iii_iv_1_1' => 'node-country-iii_iv_1_1',
            'node-country-iii_iv_1_2' => 'node-country-iii_iv_1_2',
            'node-country-iii_iv_1_3' => 'node-country-iii_iv_1_3',
            'node-country-iii_iv_1_4' => 'node-country-iii_iv_1_4',
            'node-country-iii_iv_1_5' => 'node-country-iii_iv_1_5',
            'node-country-iii_iv_1_6' => 'node-country-iii_iv_1_6',
            'node-country-iii_iv_1_7' => 'node-country-iii_iv_1_7',
            'node-country-iii_iv_1_8' => 'node-country-iii_iv_1_8',
            'node-country-iii_iv_1_9' => 'node-country-iii_iv_1_9',
            'node-country-iii_iv_1_10' => 'node-country-iii_iv_1_10',
            'node-country-iii_iv_1_11' => 'node-country-iii_iv_1_11',
            'node-country-iii_iv_1_12' => 'node-country-iii_iv_1_12',
            'node-country-iii_iv_1_13' => 'node-country-iii_iv_1_13',
            'node-country-iii_iv_1_14' => 'node-country-iii_iv_1_14',
            'node-country-iii_iv_1_15' => 'node-country-iii_iv_1_15',
            'node-country-iii_iv_1_16' => 'node-country-iii_iv_1_16',
            'node-country-iii_iv_1_17' => 'node-country-iii_iv_1_17',
            'node-country-iii_iv_2_1' => 'node-country-iii_iv_2_1',
            'node-country-iii_iv_2_2' => 'node-country-iii_iv_2_2',
            'node-country-iii_iv_2_3' => 'node-country-iii_iv_2_3',
            'node-country-iii_iv_2_3_1' => 'node-country-iii_iv_2_3_1',
            'node-country-iii_iv_2_4' => 'node-country-iii_iv_2_4',
            'node-country-iii_iv_2_5' => 'node-country-iii_iv_2_5',
            'node-country-iii_iv_2_6' => 'node-country-iii_iv_2_6',
            'node-country-iii_iv_2_7' => 'node-country-iii_iv_2_7',
            'node-country-iii_iv_2_8' => 'node-country-iii_iv_2_8',
            'node-country-iii_iv_2_9' => 'node-country-iii_iv_2_9',
            'node-country-iii_iii_1_3' => 'node-country-iii_iii_1_3',
            'node-country-iii_iii_2_3' => 'node-country-iii_iii_2_3',
            'node-country-iii_iii_2_4' => 'node-country-iii_iii_2_4',
            'node-country-iii_iii_2_5' => 'node-country-iii_iii_2_5',
            'node-country-iii_iii_2_6' => 'node-country-iii_iii_2_6',
            'node-country-iii_iii_2_7' => 'node-country-iii_iii_2_7',
            'node-country-iii_iii_2_8' => 'node-country-iii_iii_2_8',
            'node-country-iii_iii_2_9' => 'node-country-iii_iii_2_9',
            'node-country-iii_iii_2_10' => 'node-country-iii_iii_2_10',
            'node-country-iii_iii_2_11' => 'node-country-iii_iii_2_11',
            'node-country-iii_iii_2_12' => 'node-country-iii_iii_2_12',
            'node-country-iii_iii_2_13' => 'node-country-iii_iii_2_13',
            'node-country-iii_iii_2_14' => 'node-country-iii_iii_2_14',
            'node-country-iii_iii_2_15' => 'node-country-iii_iii_2_15',
            'node-country-iii_iii_2_16' => 'node-country-iii_iii_2_16',
            'node-country-iii_iii_2_17' => 'node-country-iii_iii_2_17',
            'node-country-iii_iii_2_18' => 'node-country-iii_iii_2_18',
            'node-country-iii_iii_2_19' => 'node-country-iii_iii_2_19',
            'node-country-iii_iii_3_1' => 'node-country-iii_iii_3_1',
            'node-country-iii_iii_2_1' => 'node-country-iii_iii_2_1',
            'node-country-iii_iii_2_2' => 'node-country-iii_iii_2_2',
            'node-country-iii_iii_4_1' => 'node-country-iii_iii_4_1',
            'node-country-iii_iii_4_2' => 'node-country-iii_iii_4_2',
            'node-country-field_apprehending_authorities' => 'node-country-field_apprehending_authorities',
            'node-country-field_iii_iii_1_1' => 'node-country-field_iii_iii_1_1',
            'node-country-field_iii_iii_1_4x' => 'node-country-field_iii_iii_1_4x',
          ),
          'field_base' => 
          array (
            'body' => 'body',
          ),
        ),
        'project path' => 'profiles/gdp_profile_v1_0/modules/custom',
        'description' => '',
        'version' => NULL,
      ),
      'project' => '',
      'version' => NULL,
    ),
    'ctools' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/ctools/ctools.module',
      'name' => 'ctools',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '6008',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Chaos tools',
        'description' => 'A library of helpful tools by Merlin of Chaos.',
        'core' => '7.x',
        'package' => 'Chaos tool suite',
        'files' => 
        array (
          0 => 'includes/context.inc',
          1 => 'includes/math-expr.inc',
          2 => 'includes/stylizer.inc',
        ),
        'version' => '7.x-1.3',
        'project' => 'ctools',
        'datestamp' => '1365013512',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'ctools',
      'version' => '7.x-1.3',
    ),
    'ctools_access_ruleset' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/ctools/ctools_access_ruleset/ctools_access_ruleset.module',
      'name' => 'ctools_access_ruleset',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Custom rulesets',
        'description' => 'Create custom, exportable, reusable access rulesets for applications like Panels.',
        'core' => '7.x',
        'package' => 'Chaos tool suite',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'version' => '7.x-1.3',
        'project' => 'ctools',
        'datestamp' => '1365013512',
        'php' => '5.2.4',
      ),
      'project' => 'ctools',
      'version' => '7.x-1.3',
    ),
    'ctools_ajax_sample' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/ctools/ctools_ajax_sample/ctools_ajax_sample.module',
      'name' => 'ctools_ajax_sample',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Chaos Tools (CTools) AJAX Example',
        'description' => 'Shows how to use the power of Chaos AJAX.',
        'package' => 'Chaos tool suite',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'core' => '7.x',
        'version' => '7.x-1.3',
        'project' => 'ctools',
        'datestamp' => '1365013512',
        'php' => '5.2.4',
      ),
      'project' => 'ctools',
      'version' => '7.x-1.3',
    ),
    'ctools_custom_content' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/ctools/ctools_custom_content/ctools_custom_content.module',
      'name' => 'ctools_custom_content',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Custom content panes',
        'description' => 'Create custom, exportable, reusable content panes for applications like Panels.',
        'core' => '7.x',
        'package' => 'Chaos tool suite',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'version' => '7.x-1.3',
        'project' => 'ctools',
        'datestamp' => '1365013512',
        'php' => '5.2.4',
      ),
      'project' => 'ctools',
      'version' => '7.x-1.3',
    ),
    'ctools_export_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/ctools/tests/ctools_export_test/ctools_export_test.module',
      'name' => 'ctools_export_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'CTools export test',
        'description' => 'CTools export test module',
        'core' => '7.x',
        'package' => 'Chaos tool suite',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'hidden' => true,
        'files' => 
        array (
          0 => 'ctools_export.test',
        ),
        'version' => '7.x-1.3',
        'project' => 'ctools',
        'datestamp' => '1365013512',
        'php' => '5.2.4',
      ),
      'project' => 'ctools',
      'version' => '7.x-1.3',
    ),
    'ctools_plugin_example' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/ctools/ctools_plugin_example/ctools_plugin_example.module',
      'name' => 'ctools_plugin_example',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Chaos Tools (CTools) Plugin Example',
        'description' => 'Shows how an external module can provide ctools plugins (for Panels, etc.).',
        'package' => 'Chaos tool suite',
        'dependencies' => 
        array (
          0 => 'ctools',
          1 => 'panels',
          2 => 'page_manager',
          3 => 'advanced_help',
        ),
        'core' => '7.x',
        'version' => '7.x-1.3',
        'project' => 'ctools',
        'datestamp' => '1365013512',
        'php' => '5.2.4',
      ),
      'project' => 'ctools',
      'version' => '7.x-1.3',
    ),
    'ctools_plugin_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/ctools/tests/ctools_plugin_test.module',
      'name' => 'ctools_plugin_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Chaos tools plugins test',
        'description' => 'Provides hooks for testing ctools plugins.',
        'package' => 'Chaos tool suite',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'files' => 
        array (
          0 => 'ctools.plugins.test',
          1 => 'object_cache.test',
          2 => 'css.test',
          3 => 'context.test',
          4 => 'math_expression.test',
          5 => 'math_expression_stack.test',
        ),
        'hidden' => true,
        'version' => '7.x-1.3',
        'project' => 'ctools',
        'datestamp' => '1365013512',
        'php' => '5.2.4',
      ),
      'project' => 'ctools',
      'version' => '7.x-1.3',
    ),
    'dashboard' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/dashboard/dashboard.module',
      'name' => 'dashboard',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Dashboard',
        'description' => 'Provides a dashboard page in the administrative interface for organizing administrative tasks and tracking information within your site.',
        'core' => '7.x',
        'package' => 'Core',
        'version' => '7.22',
        'files' => 
        array (
          0 => 'dashboard.test',
        ),
        'dependencies' => 
        array (
          0 => 'block',
        ),
        'configure' => 'admin/dashboard/customize',
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'database_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/database_test.module',
      'name' => 'database_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Database Test',
        'description' => 'Support module for Database layer tests.',
        'core' => '7.x',
        'package' => 'Testing',
        'version' => '7.22',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'date' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/date/date.module',
      'name' => 'date',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7004',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date',
        'description' => 'Makes date/time fields available.',
        'dependencies' => 
        array (
          0 => 'date_api',
        ),
        'package' => 'Date/Time',
        'core' => '7.x',
        'php' => '5.2',
        'files' => 
        array (
          0 => 'tests/date_api.test',
          1 => 'tests/date.test',
          2 => 'tests/date_field.test',
          3 => 'tests/date_validation.test',
          4 => 'tests/date_timezone.test',
        ),
        'version' => '7.x-2.6',
        'project' => 'date',
        'datestamp' => '1344850024',
      ),
      'project' => 'date',
      'version' => '7.x-2.6',
    ),
    'date_all_day' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/date/date_all_day/date_all_day.module',
      'name' => 'date_all_day',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date All Day',
        'description' => 'Adds \'All Day\' functionality to date fields, including an \'All Day\' theme and \'All Day\' checkboxes for the Date select and Date popup widgets.',
        'dependencies' => 
        array (
          0 => 'date_api',
          1 => 'date',
        ),
        'package' => 'Date/Time',
        'core' => '7.x',
        'version' => '7.x-2.6',
        'project' => 'date',
        'datestamp' => '1344850024',
        'php' => '5.2.4',
      ),
      'project' => 'date',
      'version' => '7.x-2.6',
    ),
    'date_api' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/date/date_api/date_api.module',
      'name' => 'date_api',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7001',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date API',
        'description' => 'A Date API that can be used by other modules.',
        'package' => 'Date/Time',
        'core' => '7.x',
        'php' => '5.2',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'date.css',
          ),
        ),
        'files' => 
        array (
          0 => 'date_api.module',
          1 => 'date_api_sql.inc',
        ),
        'version' => '7.x-2.6',
        'project' => 'date',
        'datestamp' => '1344850024',
        'dependencies' => 
        array (
        ),
      ),
      'project' => 'date',
      'version' => '7.x-2.6',
    ),
    'date_context' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/date/date_context/date_context.module',
      'name' => 'date_context',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date Context',
        'description' => 'Adds an option to the Context module to set a context condition based on the value of a date field.',
        'package' => 'Date/Time',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'date',
          1 => 'context',
        ),
        'files' => 
        array (
          0 => 'date_context.module',
          1 => 'plugins/date_context_date_condition.inc',
        ),
        'version' => '7.x-2.6',
        'project' => 'date',
        'datestamp' => '1344850024',
        'php' => '5.2.4',
      ),
      'project' => 'date',
      'version' => '7.x-2.6',
    ),
    'date_migrate' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/date/date_migrate/date_migrate.module',
      'name' => 'date_migrate',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date Migration',
        'description' => 'Provides support for importing into date fields with the Migrate module.',
        'core' => '7.x',
        'package' => 'Date/Time',
        'dependencies' => 
        array (
          0 => 'migrate',
          1 => 'date',
        ),
        'files' => 
        array (
          0 => 'date.migrate.inc',
          1 => 'date_migrate.test',
        ),
        'version' => '7.x-2.6',
        'project' => 'date',
        'datestamp' => '1344850024',
        'php' => '5.2.4',
      ),
      'project' => 'date',
      'version' => '7.x-2.6',
    ),
    'date_migrate_example' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/date/date_migrate/date_migrate_example/date_migrate_example.module',
      'name' => 'date_migrate_example',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'date',
          1 => 'date_repeat',
          2 => 'date_repeat_field',
          3 => 'date_migrate',
          4 => 'features',
          5 => 'migrate',
        ),
        'description' => 'Examples of migrating with the Date module',
        'features' => 
        array (
          'field' => 
          array (
            0 => 'node-date_migrate_example-body',
            1 => 'node-date_migrate_example-field_date',
            2 => 'node-date_migrate_example-field_date_range',
            3 => 'node-date_migrate_example-field_date_repeat',
            4 => 'node-date_migrate_example-field_datestamp',
            5 => 'node-date_migrate_example-field_datestamp_range',
            6 => 'node-date_migrate_example-field_datetime',
            7 => 'node-date_migrate_example-field_datetime_range',
          ),
          'node' => 
          array (
            0 => 'date_migrate_example',
          ),
        ),
        'files' => 
        array (
          0 => 'date_migrate_example.migrate.inc',
        ),
        'name' => 'Date Migration Example',
        'package' => 'Features',
        'project' => 'date',
        'version' => '7.x-2.6',
        'datestamp' => '1344850024',
        'php' => '5.2.4',
      ),
      'project' => 'date',
      'version' => '7.x-2.6',
    ),
    'date_popup' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/date/date_popup/date_popup.module',
      'name' => 'date_popup',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date Popup',
        'description' => 'Enables jquery popup calendars and time entry widgets for selecting dates and times.',
        'dependencies' => 
        array (
          0 => 'date_api',
        ),
        'package' => 'Date/Time',
        'core' => '7.x',
        'configure' => 'admin/config/date/date_popup',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'themes/datepicker.1.7.css',
          ),
        ),
        'version' => '7.x-2.6',
        'project' => 'date',
        'datestamp' => '1344850024',
        'php' => '5.2.4',
      ),
      'project' => 'date',
      'version' => '7.x-2.6',
    ),
    'date_repeat' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/date/date_repeat/date_repeat.module',
      'name' => 'date_repeat',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date Repeat API',
        'description' => 'A Date Repeat API to calculate repeating dates and times from iCal rules.',
        'dependencies' => 
        array (
          0 => 'date_api',
        ),
        'package' => 'Date/Time',
        'core' => '7.x',
        'php' => '5.2',
        'files' => 
        array (
          0 => 'tests/date_repeat.test',
          1 => 'tests/date_repeat_form.test',
        ),
        'version' => '7.x-2.6',
        'project' => 'date',
        'datestamp' => '1344850024',
      ),
      'project' => 'date',
      'version' => '7.x-2.6',
    ),
    'date_repeat_field' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/date/date_repeat_field/date_repeat_field.module',
      'name' => 'date_repeat_field',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date Repeat Field',
        'description' => 'Creates the option of Repeating date fields and manages Date fields that use the Date Repeat API.',
        'dependencies' => 
        array (
          0 => 'date_api',
          1 => 'date',
          2 => 'date_repeat',
        ),
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'date_repeat_field.css',
          ),
        ),
        'package' => 'Date/Time',
        'core' => '7.x',
        'version' => '7.x-2.6',
        'project' => 'date',
        'datestamp' => '1344850024',
        'php' => '5.2.4',
      ),
      'project' => 'date',
      'version' => '7.x-2.6',
    ),
    'date_tools' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/date/date_tools/date_tools.module',
      'name' => 'date_tools',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date Tools',
        'description' => 'Tools to import and auto-create dates and calendars.',
        'dependencies' => 
        array (
          0 => 'date',
        ),
        'package' => 'Date/Time',
        'core' => '7.x',
        'configure' => 'admin/config/date/tools',
        'files' => 
        array (
          0 => 'tests/date_tools.test',
        ),
        'version' => '7.x-2.6',
        'project' => 'date',
        'datestamp' => '1344850024',
        'php' => '5.2.4',
      ),
      'project' => 'date',
      'version' => '7.x-2.6',
    ),
    'date_views' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/date/date_views/date_views.module',
      'name' => 'date_views',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date Views',
        'description' => 'Views integration for date fields and date functionality.',
        'package' => 'Date/Time',
        'dependencies' => 
        array (
          0 => 'date_api',
          1 => 'views',
        ),
        'core' => '7.x',
        'php' => '5.2',
        'files' => 
        array (
          0 => 'includes/date_views_argument_handler.inc',
          1 => 'includes/date_views_argument_handler_simple.inc',
          2 => 'includes/date_views_filter_handler.inc',
          3 => 'includes/date_views_filter_handler_simple.inc',
          4 => 'includes/date_views.views_default.inc',
          5 => 'includes/date_views.views.inc',
          6 => 'includes/date_views_plugin_pager.inc',
        ),
        'version' => '7.x-2.6',
        'project' => 'date',
        'datestamp' => '1344850024',
      ),
      'project' => 'date',
      'version' => '7.x-2.6',
    ),
    'dblog' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/dblog/dblog.module',
      'name' => 'dblog',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => '7002',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Database logging',
        'description' => 'Logs and records system events to the database.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'dblog.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'detention_centre' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22',
      'name' => 'detention_centre',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '5.2.4',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'devel' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/devel/devel.module',
      'name' => 'devel',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '1',
      'schema_version' => '7004',
      'weight' => '88',
      'info' => 
      array (
        'name' => 'Devel',
        'description' => 'Various blocks, pages, and functions for developers.',
        'package' => 'Development',
        'core' => '7.x',
        'configure' => 'admin/config/development/devel',
        'tags' => 
        array (
          0 => 'developer',
        ),
        'files' => 
        array (
          0 => 'devel.test',
          1 => 'devel.mail.inc',
        ),
        'version' => '7.x-1.3',
        'project' => 'devel',
        'datestamp' => '1338940281',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'devel',
      'version' => '7.x-1.3',
    ),
    'devel_generate' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/devel/devel_generate/devel_generate.module',
      'name' => 'devel_generate',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Devel generate',
        'description' => 'Generate dummy users, nodes, and taxonomy terms.',
        'package' => 'Development',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'devel',
        ),
        'tags' => 
        array (
          0 => 'developer',
        ),
        'configure' => 'admin/config/development/generate',
        'version' => '7.x-1.3',
        'project' => 'devel',
        'datestamp' => '1338940281',
        'php' => '5.2.4',
      ),
      'project' => 'devel',
      'version' => '7.x-1.3',
    ),
    'devel_node_access' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/devel/devel_node_access.module',
      'name' => 'devel_node_access',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Devel node access',
        'description' => 'Developer blocks and page illustrating relevant node_access records.',
        'package' => 'Development',
        'dependencies' => 
        array (
          0 => 'menu',
        ),
        'core' => '7.x',
        'configure' => 'admin/config/development/devel',
        'tags' => 
        array (
          0 => 'developer',
        ),
        'version' => '7.x-1.3',
        'project' => 'devel',
        'datestamp' => '1338940281',
        'php' => '5.2.4',
      ),
      'project' => 'devel',
      'version' => '7.x-1.3',
    ),
    'diff' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/diff/diff.module',
      'name' => 'diff',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => '7305',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Diff',
        'description' => 'Show differences between content revisions.',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'DiffEngine.php',
        ),
        'version' => '7.x-3.2',
        'project' => 'diff',
        'datestamp' => '1352784357',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'diff',
      'version' => '7.x-3.2',
    ),
    'drupal_system_listing_compatible_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/drupal_system_listing_compatible_test/drupal_system_listing_compatible_test.module',
      'name' => 'drupal_system_listing_compatible_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Drupal system listing compatible test',
        'description' => 'Support module for testing the drupal_system_listing function.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'drupal_system_listing_incompatible_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/drupal_system_listing_incompatible_test/drupal_system_listing_incompatible_test.module',
      'name' => 'drupal_system_listing_incompatible_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Drupal system listing incompatible test',
        'description' => 'Support module for testing the drupal_system_listing function.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'ds' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/ds/ds.module',
      'name' => 'ds',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7201',
      'weight' => '1',
      'info' => 
      array (
        'name' => 'Display Suite',
        'description' => 'Extend the display options for every entity type.',
        'core' => '7.x',
        'package' => 'Display Suite',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'files' => 
        array (
          0 => 'views/views_plugin_ds_entity_view.inc',
          1 => 'views/views_plugin_ds_fields_view.inc',
          2 => 'tests/ds.base.test',
          3 => 'tests/ds.search.test',
          4 => 'tests/ds.entities.test',
          5 => 'tests/ds.exportables.test',
          6 => 'tests/ds.views.test',
          7 => 'tests/ds.forms.test',
        ),
        'configure' => 'admin/structure/ds',
        'version' => '7.x-2.2',
        'project' => 'ds',
        'datestamp' => '1361483273',
        'php' => '5.2.4',
      ),
      'project' => 'ds',
      'version' => '7.x-2.2',
    ),
    'ds_devel' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/ds/modules/ds_devel/ds_devel.module',
      'name' => 'ds_devel',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Display Suite Devel',
        'description' => 'Development functionality for Display Suite.',
        'core' => '7.x',
        'package' => 'Display Suite',
        'dependencies' => 
        array (
          0 => 'ds',
          1 => 'devel',
        ),
        'version' => '7.x-2.2',
        'project' => 'ds',
        'datestamp' => '1361483273',
        'php' => '5.2.4',
      ),
      'project' => 'ds',
      'version' => '7.x-2.2',
    ),
    'ds_exportables_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/ds/tests/ds_exportables_test/ds_exportables_test.module',
      'name' => 'ds_exportables_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Display Suite exportables test',
        'description' => 'Tests for exportables with Display Suite.',
        'package' => 'Display Suite',
        'core' => '7.x',
        'hidden' => true,
        'version' => '7.x-2.2',
        'project' => 'ds',
        'datestamp' => '1361483273',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'ds',
      'version' => '7.x-2.2',
    ),
    'ds_extras' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/ds/modules/ds_extras/ds_extras.module',
      'name' => 'ds_extras',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7200',
      'weight' => '2',
      'info' => 
      array (
        'name' => 'Display Suite Extras',
        'description' => 'Contains additional features for Display Suite.',
        'core' => '7.x',
        'package' => 'Display Suite',
        'dependencies' => 
        array (
          0 => 'ds',
        ),
        'configure' => 'admin/structure/ds/list/extras',
        'version' => '7.x-2.2',
        'project' => 'ds',
        'datestamp' => '1361483273',
        'php' => '5.2.4',
      ),
      'project' => 'ds',
      'version' => '7.x-2.2',
    ),
    'ds_format' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/ds/modules/ds_format/ds_format.module',
      'name' => 'ds_format',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Display Suite Format',
        'description' => 'Provides the Display Suite Code format filter.',
        'core' => '7.x',
        'package' => 'Display Suite',
        'dependencies' => 
        array (
          0 => 'ds',
        ),
        'configure' => 'admin/structure/ds/list/extras',
        'version' => '7.x-2.2',
        'project' => 'ds',
        'datestamp' => '1361483273',
        'php' => '5.2.4',
      ),
      'project' => 'ds',
      'version' => '7.x-2.2',
    ),
    'ds_forms' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/ds/modules/ds_forms/ds_forms.module',
      'name' => 'ds_forms',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '20',
      'info' => 
      array (
        'name' => 'Display Suite Forms',
        'description' => 'Manage the layout of forms in Display Suite.',
        'core' => '7.x',
        'package' => 'Display Suite',
        'dependencies' => 
        array (
          0 => 'ds',
        ),
        'version' => '7.x-2.2',
        'project' => 'ds',
        'datestamp' => '1361483273',
        'php' => '5.2.4',
      ),
      'project' => 'ds',
      'version' => '7.x-2.2',
    ),
    'ds_search' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/ds/modules/ds_search/ds_search.module',
      'name' => 'ds_search',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Display Suite Search',
        'description' => 'Extend the display options for search results for Drupal Core or Apache Solr.',
        'core' => '7.x',
        'package' => 'Display Suite',
        'dependencies' => 
        array (
          0 => 'ds',
        ),
        'configure' => 'admin/structure/ds/list/search',
        'version' => '7.x-2.2',
        'project' => 'ds',
        'datestamp' => '1361483273',
        'php' => '5.2.4',
      ),
      'project' => 'ds',
      'version' => '7.x-2.2',
    ),
    'ds_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/ds/tests/ds_test.module',
      'name' => 'ds_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Display Suite Test',
        'description' => 'Test module for Display Suite',
        'core' => '7.x',
        'package' => 'Display Suite',
        'dependencies' => 
        array (
          0 => 'ds_extras',
        ),
        'hidden' => true,
        'version' => '7.x-2.2',
        'project' => 'ds',
        'datestamp' => '1361483273',
        'php' => '5.2.4',
      ),
      'project' => 'ds',
      'version' => '7.x-2.2',
    ),
    'ds_ui' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/ds/modules/ds_ui/ds_ui.module',
      'name' => 'ds_ui',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Display Suite UI',
        'description' => 'User interface for managing fields, view modes and classes.',
        'core' => '7.x',
        'package' => 'Display Suite',
        'dependencies' => 
        array (
          0 => 'ds',
        ),
        'version' => '7.x-2.2',
        'project' => 'ds',
        'datestamp' => '1361483273',
        'php' => '5.2.4',
      ),
      'project' => 'ds',
      'version' => '7.x-2.2',
    ),
    'edit' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/edit/edit.module',
      'name' => 'edit',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Edit',
        'description' => 'In-place content editing.',
        'package' => 'Spark',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'entity',
          1 => 'field (>=7.22)',
        ),
        'version' => '7.x-1.0-alpha11+0-dev',
        'project' => 'edit',
        'datestamp' => '1366419210',
        'php' => '5.2.4',
      ),
      'project' => 'edit',
      'version' => '7.x-1.0-alpha11+0-dev',
    ),
    'empty_fields' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/empty_fields/empty_fields.module',
      'name' => 'empty_fields',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Empty Fields',
        'description' => 'Provides options for displaying empty fields.',
        'core' => '7.x',
        'package' => 'Fields',
        'files' => 
        array (
          0 => 'plugins/empty_fields_handler.inc',
          1 => 'plugins/empty_fields_handler_text.inc',
        ),
        'dependencies' => 
        array (
          0 => 'field',
          1 => 'field_formatter_settings',
        ),
        'version' => '7.x-2.0-rc1',
        'project' => 'empty_fields',
        'datestamp' => '1352905605',
        'php' => '5.2.4',
      ),
      'project' => 'empty_fields',
      'version' => '7.x-2.0-rc1',
    ),
    'entity' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/entity/entity.module',
      'name' => 'entity',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7002',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Entity API',
        'description' => 'Enables modules to work with any entity type and to provide entities.',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'entity.features.inc',
          1 => 'entity.i18n.inc',
          2 => 'entity.info.inc',
          3 => 'entity.rules.inc',
          4 => 'entity.test',
          5 => 'includes/entity.inc',
          6 => 'includes/entity.controller.inc',
          7 => 'includes/entity.ui.inc',
          8 => 'includes/entity.wrapper.inc',
          9 => 'views/entity.views.inc',
          10 => 'views/handlers/entity_views_field_handler_helper.inc',
          11 => 'views/handlers/entity_views_handler_area_entity.inc',
          12 => 'views/handlers/entity_views_handler_field_boolean.inc',
          13 => 'views/handlers/entity_views_handler_field_date.inc',
          14 => 'views/handlers/entity_views_handler_field_duration.inc',
          15 => 'views/handlers/entity_views_handler_field_entity.inc',
          16 => 'views/handlers/entity_views_handler_field_field.inc',
          17 => 'views/handlers/entity_views_handler_field_numeric.inc',
          18 => 'views/handlers/entity_views_handler_field_options.inc',
          19 => 'views/handlers/entity_views_handler_field_text.inc',
          20 => 'views/handlers/entity_views_handler_field_uri.inc',
          21 => 'views/handlers/entity_views_handler_relationship_by_bundle.inc',
          22 => 'views/handlers/entity_views_handler_relationship.inc',
          23 => 'views/plugins/entity_views_plugin_row_entity_view.inc',
        ),
        'version' => '7.x-1.0',
        'project' => 'entity',
        'datestamp' => '1356471145',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'entity',
      'version' => '7.x-1.0',
    ),
    'entityreference' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/entityreference/entityreference.module',
      'name' => 'entityreference',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7002',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Entity Reference',
        'description' => 'Provides a field that can reference other entities.',
        'core' => '7.x',
        'package' => 'Fields',
        'dependencies' => 
        array (
          0 => 'entity',
          1 => 'ctools',
        ),
        'files' => 
        array (
          0 => 'entityreference.migrate.inc',
          1 => 'plugins/selection/abstract.inc',
          2 => 'plugins/selection/views.inc',
          3 => 'plugins/behavior/abstract.inc',
          4 => 'views/entityreference_plugin_display.inc',
          5 => 'views/entityreference_plugin_style.inc',
          6 => 'views/entityreference_plugin_row_fields.inc',
          7 => 'tests/entityreference.handlers.test',
          8 => 'tests/entityreference.taxonomy.test',
          9 => 'tests/entityreference.admin.test',
        ),
        'version' => '7.x-1.0',
        'project' => 'entityreference',
        'datestamp' => '1353230808',
        'php' => '5.2.4',
      ),
      'project' => 'entityreference',
      'version' => '7.x-1.0',
    ),
    'entityreference_behavior_example' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/entityreference/examples/entityreference_behavior_example/entityreference_behavior_example.module',
      'name' => 'entityreference_behavior_example',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Entity Reference Behavior Example',
        'description' => 'Provides some example code for implementing Entity Reference behaviors.',
        'core' => '7.x',
        'package' => 'Fields',
        'dependencies' => 
        array (
          0 => 'entityreference',
        ),
        'version' => '7.x-1.0',
        'project' => 'entityreference',
        'datestamp' => '1353230808',
        'php' => '5.2.4',
      ),
      'project' => 'entityreference',
      'version' => '7.x-1.0',
    ),
    'entity_cache_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/entity_cache_test.module',
      'name' => 'entity_cache_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Entity cache test',
        'description' => 'Support module for testing entity cache.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'entity_cache_test_dependency',
        ),
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'entity_cache_test_dependency' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/entity_cache_test_dependency.module',
      'name' => 'entity_cache_test_dependency',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Entity cache test dependency',
        'description' => 'Support dependency module for testing entity cache.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'entity_crud_hook_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/entity_crud_hook_test.module',
      'name' => 'entity_crud_hook_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Entity CRUD Hooks Test',
        'description' => 'Support module for CRUD hook tests.',
        'core' => '7.x',
        'package' => 'Testing',
        'version' => '7.22',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'entity_feature' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/entity/tests/entity_feature.module',
      'name' => 'entity_feature',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Entity feature module',
        'description' => 'Provides some entities in code.',
        'version' => '7.x-1.0',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'entity_feature.module',
        ),
        'dependencies' => 
        array (
          0 => 'entity_test',
        ),
        'hidden' => true,
        'project' => 'entity',
        'datestamp' => '1356471145',
        'php' => '5.2.4',
      ),
      'project' => 'entity',
      'version' => '7.x-1.0',
    ),
    'entity_query_access_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/entity_query_access_test.module',
      'name' => 'entity_query_access_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Entity query access test',
        'description' => 'Support module for checking entity query results.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'entity_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/entity/tests/entity_test.module',
      'name' => 'entity_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Entity CRUD test module',
        'description' => 'Provides entity types based upon the CRUD API.',
        'version' => '7.x-1.0',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'entity_test.module',
          1 => 'entity_test.install',
        ),
        'dependencies' => 
        array (
          0 => 'entity',
        ),
        'hidden' => true,
        'project' => 'entity',
        'datestamp' => '1356471145',
        'php' => '5.2.4',
      ),
      'project' => 'entity',
      'version' => '7.x-1.0',
    ),
    'entity_test_i18n' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/entity/tests/entity_test_i18n.module',
      'name' => 'entity_test_i18n',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Entity-test type translation',
        'description' => 'Allows translating entity-test types.',
        'dependencies' => 
        array (
          0 => 'entity_test',
          1 => 'i18n_string',
        ),
        'package' => 'Multilingual - Internationalization',
        'core' => '7.x',
        'hidden' => true,
        'version' => '7.x-1.0',
        'project' => 'entity',
        'datestamp' => '1356471145',
        'php' => '5.2.4',
      ),
      'project' => 'entity',
      'version' => '7.x-1.0',
    ),
    'entity_token' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/entity/entity_token.module',
      'name' => 'entity_token',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Entity tokens',
        'description' => 'Provides token replacements for all properties that have no tokens and are known to the entity API.',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'entity_token.tokens.inc',
          1 => 'entity_token.module',
        ),
        'dependencies' => 
        array (
          0 => 'entity',
        ),
        'version' => '7.x-1.0',
        'project' => 'entity',
        'datestamp' => '1356471145',
        'php' => '5.2.4',
      ),
      'project' => 'entity',
      'version' => '7.x-1.0',
    ),
    'error_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/error_test.module',
      'name' => 'error_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Error test',
        'description' => 'Support module for error and exception testing.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'eva' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/eva/eva.module',
      'name' => 'eva',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Eva',
        'description' => 'Provides a Views display type that can be attached to entities.',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'views',
        ),
        'package' => 'Views',
        'files' => 
        array (
          0 => 'eva_plugin_display_entity.inc',
        ),
        'version' => '7.x-1.2',
        'project' => 'eva',
        'datestamp' => '1343701935',
        'php' => '5.2.4',
      ),
      'project' => 'eva',
      'version' => '7.x-1.2',
    ),
    'features' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/features/features.module',
      'name' => 'features',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '6101',
      'weight' => '20',
      'info' => 
      array (
        'name' => 'Features',
        'description' => 'Provides feature management for Drupal.',
        'core' => '7.x',
        'package' => 'Features',
        'files' => 
        array (
          0 => 'tests/features.test',
        ),
        'version' => '7.x-2.0-beta2',
        'project' => 'features',
        'datestamp' => '1364589018',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'features',
      'version' => '7.x-2.0-beta2',
    ),
    'features_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/features/tests/features_test.module',
      'name' => 'features_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Features Tests',
        'description' => 'Test module for Features testing.',
        'core' => '7.x',
        'package' => 'Testing',
        'php' => '5.2.0',
        'dependencies' => 
        array (
          0 => 'features',
          1 => 'image',
          2 => 'strongarm',
          3 => 'taxonomy',
          4 => 'views',
        ),
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'strongarm:strongarm:1',
            1 => 'views:views_default:3.0',
          ),
          'features_api' => 
          array (
            0 => 'api:1',
          ),
          'field' => 
          array (
            0 => 'node-features_test-field_features_test',
          ),
          'filter' => 
          array (
            0 => 'features_test',
          ),
          'image' => 
          array (
            0 => 'features_test',
          ),
          'node' => 
          array (
            0 => 'features_test',
          ),
          'taxonomy' => 
          array (
            0 => 'taxonomy_features_test',
          ),
          'user_permission' => 
          array (
            0 => 'create features_test content',
          ),
          'views_view' => 
          array (
            0 => 'features_test',
          ),
        ),
        'hidden' => true,
        'version' => '7.x-2.0-beta2',
        'project' => 'features',
        'datestamp' => '1364589018',
      ),
      'project' => 'features',
      'version' => '7.x-2.0-beta2',
    ),
    'feeds' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/feeds/feeds.module',
      'name' => 'feeds',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '1',
      'schema_version' => '7208',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Feeds',
        'description' => 'Aggregates RSS/Atom/RDF feeds, imports CSV files and more.',
        'package' => 'Feeds',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'ctools',
          1 => 'job_scheduler',
        ),
        'files' => 
        array (
          0 => 'includes/FeedsConfigurable.inc',
          1 => 'includes/FeedsImporter.inc',
          2 => 'includes/FeedsSource.inc',
          3 => 'libraries/ParserCSV.inc',
          4 => 'libraries/http_request.inc',
          5 => 'libraries/PuSHSubscriber.inc',
          6 => 'plugins/FeedsCSVParser.inc',
          7 => 'plugins/FeedsFetcher.inc',
          8 => 'plugins/FeedsFileFetcher.inc',
          9 => 'plugins/FeedsHTTPFetcher.inc',
          10 => 'plugins/FeedsNodeProcessor.inc',
          11 => 'plugins/FeedsOPMLParser.inc',
          12 => 'plugins/FeedsParser.inc',
          13 => 'plugins/FeedsPlugin.inc',
          14 => 'plugins/FeedsProcessor.inc',
          15 => 'plugins/FeedsSimplePieParser.inc',
          16 => 'plugins/FeedsSitemapParser.inc',
          17 => 'plugins/FeedsSyndicationParser.inc',
          18 => 'plugins/FeedsTermProcessor.inc',
          19 => 'plugins/FeedsUserProcessor.inc',
          20 => 'tests/feeds.test',
          21 => 'tests/feeds_date_time.test',
          22 => 'tests/feeds_mapper_date.test',
          23 => 'tests/feeds_mapper_date_multiple.test',
          24 => 'tests/feeds_mapper_field.test',
          25 => 'tests/feeds_mapper_file.test',
          26 => 'tests/feeds_mapper_path.test',
          27 => 'tests/feeds_mapper_profile.test',
          28 => 'tests/feeds_mapper.test',
          29 => 'tests/feeds_mapper_config.test',
          30 => 'tests/feeds_fetcher_file.test',
          31 => 'tests/feeds_processor_node.test',
          32 => 'tests/feeds_processor_term.test',
          33 => 'tests/feeds_processor_user.test',
          34 => 'tests/feeds_scheduler.test',
          35 => 'tests/feeds_mapper_link.test',
          36 => 'tests/feeds_mapper_taxonomy.test',
          37 => 'tests/parser_csv.test',
          38 => 'views/feeds_views_handler_argument_importer_id.inc',
          39 => 'views/feeds_views_handler_field_importer_name.inc',
          40 => 'views/feeds_views_handler_field_log_message.inc',
          41 => 'views/feeds_views_handler_field_severity.inc',
          42 => 'views/feeds_views_handler_field_source.inc',
          43 => 'views/feeds_views_handler_filter_severity.inc',
        ),
        'version' => '7.x-2.0-alpha8',
        'project' => 'feeds',
        'datestamp' => '1366671911',
        'php' => '5.2.4',
      ),
      'project' => 'feeds',
      'version' => '7.x-2.0-alpha8',
    ),
    'feeds_import' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/feeds/feeds_import/feeds_import.module',
      'name' => 'feeds_import',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'feeds',
        ),
        'description' => 'An example of a node importer and a user importer.',
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'feeds:feeds_importer_default:1',
          ),
          'feeds_importer' => 
          array (
            0 => 'node',
            1 => 'user',
          ),
        ),
        'files' => 
        array (
          0 => 'feeds_import.test',
        ),
        'name' => 'Feeds Import',
        'package' => 'Feeds',
        'php' => '5.2.4',
        'version' => '7.x-2.0-alpha8',
        'project' => 'feeds',
        'datestamp' => '1366671911',
      ),
      'project' => 'feeds',
      'version' => '7.x-2.0-alpha8',
    ),
    'feeds_news' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/feeds/feeds_news/feeds_news.module',
      'name' => 'feeds_news',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'features',
          1 => 'feeds',
          2 => 'views',
        ),
        'description' => 'A news aggregator built with feeds, creates nodes from imported feed items. With OPML import.',
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'feeds:feeds_importer_default:1',
            1 => 'views:views_default:3.0',
          ),
          'feeds_importer' => 
          array (
            0 => 'feed',
            1 => 'opml',
          ),
          'field' => 
          array (
            0 => 'node-feed_item-field_feed_item_description',
          ),
          'node' => 
          array (
            0 => 'feed',
            1 => 'feed_item',
          ),
          'views_view' => 
          array (
            0 => 'feeds_defaults_feed_items',
          ),
        ),
        'files' => 
        array (
          0 => 'feeds_news.module',
          1 => 'feeds_news.test',
        ),
        'name' => 'Feeds News',
        'package' => 'Feeds',
        'php' => '5.2.4',
        'version' => '7.x-2.0-alpha8',
        'project' => 'feeds',
        'datestamp' => '1366671911',
      ),
      'project' => 'feeds',
      'version' => '7.x-2.0-alpha8',
    ),
    'feeds_tamper' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/feeds_tamper/feeds_tamper.module',
      'name' => 'feeds_tamper',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7004',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Feeds Tamper',
        'description' => 'Modify feeds data before it gets saved.',
        'package' => 'Feeds',
        'dependencies' => 
        array (
          0 => 'feeds',
        ),
        'files' => 
        array (
          0 => 'tests/feeds_tamper.test',
          1 => 'tests/feeds_tamper_plugins.test',
        ),
        'core' => '7.x',
        'version' => '7.x-1.0-beta3+55-dev',
        'project' => 'feeds_tamper',
        'datestamp' => '1364000251',
        'php' => '5.2.4',
      ),
      'project' => 'feeds_tamper',
      'version' => '7.x-1.0-beta3+55-dev',
    ),
    'feeds_tamper_ui' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/feeds_tamper/feeds_tamper_ui/feeds_tamper_ui.module',
      'name' => 'feeds_tamper_ui',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Feeds Tamper Admin UI',
        'description' => 'Administrative UI for Feeds Tamper module.',
        'package' => 'Feeds',
        'dependencies' => 
        array (
          0 => 'feeds_tamper',
          1 => 'feeds_ui',
        ),
        'files' => 
        array (
          0 => 'tests/feeds_tamper_ui.test',
        ),
        'core' => '7.x',
        'version' => '7.x-1.0-beta3+55-dev',
        'project' => 'feeds_tamper',
        'datestamp' => '1364000251',
        'php' => '5.2.4',
      ),
      'project' => 'feeds_tamper',
      'version' => '7.x-1.0-beta3+55-dev',
    ),
    'feeds_tests' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/feeds/tests/feeds_tests.module',
      'name' => 'feeds_tests',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Feeds module tests',
        'description' => 'Support module for Feeds related testing.',
        'package' => 'Testing',
        'version' => '7.x-2.0-alpha8',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'feeds_test.module',
        ),
        'hidden' => true,
        'project' => 'feeds',
        'datestamp' => '1366671911',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'feeds',
      'version' => '7.x-2.0-alpha8',
    ),
    'feeds_ui' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/feeds/feeds_ui/feeds_ui.module',
      'name' => 'feeds_ui',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7000',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Feeds Admin UI',
        'description' => 'Administrative UI for Feeds module.',
        'package' => 'Feeds',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'feeds',
        ),
        'configure' => 'admin/structure/feeds',
        'files' => 
        array (
          0 => 'feeds_ui.test',
        ),
        'version' => '7.x-2.0-alpha8',
        'project' => 'feeds',
        'datestamp' => '1366671911',
        'php' => '5.2.4',
      ),
      'project' => 'feeds',
      'version' => '7.x-2.0-alpha8',
    ),
    'field' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/field/field.module',
      'name' => 'field',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7003',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Field',
        'description' => 'Field API to add fields to entities like nodes and users.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'field.module',
          1 => 'field.attach.inc',
          2 => 'field.info.class.inc',
          3 => 'tests/field.test',
        ),
        'dependencies' => 
        array (
          0 => 'field_sql_storage',
        ),
        'required' => true,
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'theme/field.css',
          ),
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'field_formatter_settings' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/field_formatter_settings/field_formatter_settings.module',
      'name' => 'field_formatter_settings',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Field formatter settings API',
        'description' => 'Provides missing alter hooks for field formatter settings and summaries',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'field_ui',
        ),
        'files' => 
        array (
          0 => 'field_formatter_settings.module',
        ),
        'version' => '7.x-1.0',
        'project' => 'field_formatter_settings',
        'datestamp' => '1345588632',
        'php' => '5.2.4',
      ),
      'project' => 'field_formatter_settings',
      'version' => '7.x-1.0',
    ),
    'field_group' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/field_group/field_group.module',
      'name' => 'field_group',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7005',
      'weight' => '1',
      'info' => 
      array (
        'name' => 'Fieldgroup',
        'description' => 'Fieldgroup',
        'package' => 'Fields',
        'dependencies' => 
        array (
          0 => 'field',
          1 => 'ctools',
        ),
        'core' => '7.x',
        'files' => 
        array (
          0 => 'field_group.install',
          1 => 'field_group.module',
          2 => 'field_group.field_ui.inc',
          3 => 'field_group.form.inc',
          4 => 'field_group.features.inc',
          5 => 'field_group.test',
        ),
        'version' => '7.x-1.1',
        'project' => 'field_group',
        'datestamp' => '1319051133',
        'php' => '5.2.4',
      ),
      'project' => 'field_group',
      'version' => '7.x-1.1',
    ),
    'field_sql_storage' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/field/modules/field_sql_storage/field_sql_storage.module',
      'name' => 'field_sql_storage',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7002',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Field SQL storage',
        'description' => 'Stores field data in an SQL database.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'field',
        ),
        'files' => 
        array (
          0 => 'field_sql_storage.test',
        ),
        'required' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'field_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/field/tests/field_test.module',
      'name' => 'field_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Field API Test',
        'description' => 'Support module for the Field API tests.',
        'core' => '7.x',
        'package' => 'Testing',
        'files' => 
        array (
          0 => 'field_test.entity.inc',
        ),
        'version' => '7.22',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'field_ui' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/field_ui/field_ui.module',
      'name' => 'field_ui',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Field UI',
        'description' => 'User interface for the Field API.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'field',
        ),
        'files' => 
        array (
          0 => 'field_ui.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'file' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/file/file.module',
      'name' => 'file',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'File',
        'description' => 'Defines a file field type.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'field',
        ),
        'files' => 
        array (
          0 => 'tests/file.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'file_module_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/file/tests/file_module_test.module',
      'name' => 'file_module_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'File test',
        'description' => 'Provides hooks for testing File module functionality.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'file_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/file_test.module',
      'name' => 'file_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'File test',
        'description' => 'Support module for file handling tests.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'file_test.module',
        ),
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'filter' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/filter/filter.module',
      'name' => 'filter',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7010',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Filter',
        'description' => 'Filters content in preparation for display.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'filter.test',
        ),
        'required' => true,
        'configure' => 'admin/config/content/formats',
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'filter_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/filter_test.module',
      'name' => 'filter_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Filter test module',
        'description' => 'Tests filter hooks and functions.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'form_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/form_test.module',
      'name' => 'form_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'FormAPI Test',
        'description' => 'Support module for Form API tests.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'forum' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/forum/forum.module',
      'name' => 'forum',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Forum',
        'description' => 'Provides discussion forums.',
        'dependencies' => 
        array (
          0 => 'taxonomy',
          1 => 'comment',
        ),
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'forum.test',
        ),
        'configure' => 'admin/structure/forum',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'forum.css',
          ),
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'gdp_create_fields' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/custom/gdp_create_fields/gdp_create_fields.module',
      'name' => 'gdp_create_fields',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Create Fields',
        'description' => 'Glue code for GDP.',
        'package' => 'Other',
        'core' => '7.x',
        'dependencies' => 
        array (
        ),
        'version' => NULL,
        'php' => '5.2.4',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'gdp_dc' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/custom/gdp_dc/gdp_dc.module',
      'name' => 'gdp_dc',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'gdp_dc',
        'description' => 'dc',
        'core' => '7.x',
        'package' => 'Features',
        'php' => '5.2.4',
        'version' => '7.x-1.0',
        'project' => 'gdp_dc',
        'dependencies' => 
        array (
          0 => 'addressfield',
          1 => 'ctools',
          2 => 'ds',
          3 => 'entityreference',
          4 => 'features',
          5 => 'field_group',
          6 => 'gdp_fields',
          7 => 'geocoder',
          8 => 'geofield',
          9 => 'geofield_map',
          10 => 'options',
          11 => 'strongarm',
          12 => 'text',
        ),
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'ds:ds:1',
            1 => 'field_group:field_group:1',
            2 => 'strongarm:strongarm:1',
          ),
          'ds_fields' => 
          array (
            0 => 'dc',
          ),
          'features_api' => 
          array (
            0 => 'api:1',
          ),
          'field_base' => 
          array (
            0 => 'field_country',
            1 => 'field_custodial_authority',
            2 => 'field_geolocation',
            3 => 'field_iv_a_5_1_new',
            4 => 'field_iv_a_6_1',
            5 => 'field_iv_b_1_1',
            6 => 'field_iv_b_1_2',
            7 => 'field_iv_c_2_1',
            8 => 'field_year_ceased_operating',
            9 => 'iv_a_2_1',
            10 => 'iv_a_3_1',
            11 => 'iv_a_3_2',
            12 => 'iv_a_4_1',
            13 => 'iv_a_4_2',
            14 => 'iv_a_5_1',
            15 => 'iv_a_8_1',
            16 => 'iv_b_10_1',
            17 => 'iv_b_11_1',
            18 => 'iv_b_11_2',
            19 => 'iv_b_11_3',
            20 => 'iv_b_12_1',
            21 => 'iv_b_12_3',
            22 => 'iv_b_13_1',
            23 => 'iv_b_2_1',
            24 => 'iv_b_2_2',
            25 => 'iv_b_3_1',
            26 => 'iv_b_3_2',
            27 => 'iv_b_3_3',
            28 => 'iv_b_4_1',
            29 => 'iv_b_5_1',
            30 => 'iv_b_6_1',
            31 => 'iv_b_6_2',
            32 => 'iv_b_6_3',
            33 => 'iv_b_6_4',
            34 => 'iv_b_7_1',
            35 => 'iv_b_7_2',
            36 => 'iv_b_7_3',
            37 => 'iv_b_7_4',
            38 => 'iv_b_7_5',
            39 => 'iv_b_7_6',
            40 => 'iv_b_8_1',
            41 => 'iv_b_8_10',
            42 => 'iv_b_8_2',
            43 => 'iv_b_8_3',
            44 => 'iv_b_8_4',
            45 => 'iv_b_8_5',
            46 => 'iv_b_8_6',
            47 => 'iv_b_8_7',
            48 => 'iv_b_8_8',
            49 => 'iv_b_8_9',
            50 => 'iv_b_9_1',
            51 => 'iv_b_9_2',
            52 => 'iv_c_3_1',
            53 => 'iv_c_3_2',
            54 => 'iv_c_4_1',
            55 => 'iv_c_4_2',
            56 => 'iv_c_5_1',
          ),
          'field_group' => 
          array (
            0 => 'group_dc_iv_a_1|node|dc|default',
            1 => 'group_dc_iv_a_group|node|dc|default',
            2 => 'group_dc_iv_a|node|dc|default',
            3 => 'group_dc_iv_b_10|node|dc|default',
            4 => 'group_dc_iv_b_11|node|dc|default',
            5 => 'group_dc_iv_b_12|node|dc|default',
            6 => 'group_dc_iv_b_13|node|dc|default',
            7 => 'group_dc_iv_b_1|node|dc|default',
            8 => 'group_dc_iv_b_2|node|dc|default',
            9 => 'group_dc_iv_b_3|node|dc|default',
            10 => 'group_dc_iv_b_4|node|dc|default',
            11 => 'group_dc_iv_b_5|node|dc|default',
            12 => 'group_dc_iv_b_6|node|dc|default',
            13 => 'group_dc_iv_b_7|node|dc|default',
            14 => 'group_dc_iv_b_8|node|dc|default',
            15 => 'group_dc_iv_b_9|node|dc|default',
            16 => 'group_dc_iv_b_group|node|dc|default',
            17 => 'group_dc_iv_b|node|dc|default',
            18 => 'group_dc_iv_c_1|node|dc|default',
            19 => 'group_dc_iv_c_2|node|dc|default',
            20 => 'group_dc_iv_c_3|node|dc|default',
            21 => 'group_dc_iv_c_4|node|dc|default',
            22 => 'group_dc_iv_c_5|node|dc|default',
            23 => 'group_dc_iv_c_group|node|dc|default',
            24 => 'group_dc_iv_c|node|dc|default',
            25 => 'group_dc_iv|node|dc|default',
            26 => 'group_location|node|dc|default',
          ),
          'field_instance' => 
          array (
            0 => 'node-dc-body',
            1 => 'node-dc-field_country',
            2 => 'node-dc-field_custodial_authority',
            3 => 'node-dc-field_geolocation',
            4 => 'node-dc-field_iv_a_5_1_new',
            5 => 'node-dc-field_iv_a_6_1',
            6 => 'node-dc-field_iv_b_1_1',
            7 => 'node-dc-field_iv_b_1_2',
            8 => 'node-dc-field_iv_c_2_1',
            9 => 'node-dc-field_year_ceased_operating',
            10 => 'node-dc-iv_a_2_1',
            11 => 'node-dc-iv_a_3_1',
            12 => 'node-dc-iv_a_3_2',
            13 => 'node-dc-iv_a_4_1',
            14 => 'node-dc-iv_a_4_2',
            15 => 'node-dc-iv_a_5_1',
            16 => 'node-dc-iv_a_8_1',
            17 => 'node-dc-iv_b_10_1',
            18 => 'node-dc-iv_b_11_1',
            19 => 'node-dc-iv_b_11_2',
            20 => 'node-dc-iv_b_11_3',
            21 => 'node-dc-iv_b_12_1',
            22 => 'node-dc-iv_b_12_3',
            23 => 'node-dc-iv_b_13_1',
            24 => 'node-dc-iv_b_2_1',
            25 => 'node-dc-iv_b_2_2',
            26 => 'node-dc-iv_b_3_1',
            27 => 'node-dc-iv_b_3_2',
            28 => 'node-dc-iv_b_3_3',
            29 => 'node-dc-iv_b_4_1',
            30 => 'node-dc-iv_b_5_1',
            31 => 'node-dc-iv_b_6_1',
            32 => 'node-dc-iv_b_6_2',
            33 => 'node-dc-iv_b_6_3',
            34 => 'node-dc-iv_b_6_4',
            35 => 'node-dc-iv_b_7_1',
            36 => 'node-dc-iv_b_7_2',
            37 => 'node-dc-iv_b_7_3',
            38 => 'node-dc-iv_b_7_4',
            39 => 'node-dc-iv_b_7_5',
            40 => 'node-dc-iv_b_7_6',
            41 => 'node-dc-iv_b_8_1',
            42 => 'node-dc-iv_b_8_10',
            43 => 'node-dc-iv_b_8_2',
            44 => 'node-dc-iv_b_8_3',
            45 => 'node-dc-iv_b_8_4',
            46 => 'node-dc-iv_b_8_5',
            47 => 'node-dc-iv_b_8_6',
            48 => 'node-dc-iv_b_8_7',
            49 => 'node-dc-iv_b_8_8',
            50 => 'node-dc-iv_b_8_9',
            51 => 'node-dc-iv_b_9_1',
            52 => 'node-dc-iv_b_9_2',
            53 => 'node-dc-iv_c_3_1',
            54 => 'node-dc-iv_c_3_2',
            55 => 'node-dc-iv_c_4_1',
            56 => 'node-dc-iv_c_4_2',
            57 => 'node-dc-iv_c_5_1',
          ),
          'node' => 
          array (
            0 => 'dc',
          ),
          'variable' => 
          array (
            0 => 'comment_anonymous_dc',
            1 => 'comment_dc',
            2 => 'comment_default_mode_dc',
            3 => 'comment_default_per_page_dc',
            4 => 'comment_form_location_dc',
            5 => 'comment_preview_dc',
            6 => 'comment_subject_field_dc',
            7 => 'field_bundle_settings_node__dc',
            8 => 'language_content_type_dc',
            9 => 'menu_options_dc',
            10 => 'menu_parent_dc',
            11 => 'node_options_dc',
            12 => 'node_preview_dc',
            13 => 'node_submitted_dc',
          ),
        ),
        'features_exclude' => 
        array (
          'field' => 
          array (
            'node-dc-body' => 'node-dc-body',
            'node-dc-iv_a_1_1' => 'node-dc-iv_a_1_1',
            'node-dc-iv_a_2_1' => 'node-dc-iv_a_2_1',
            'node-dc-iv_a_3_1' => 'node-dc-iv_a_3_1',
            'node-dc-iv_a_3_2' => 'node-dc-iv_a_3_2',
            'node-dc-iv_a_4_1' => 'node-dc-iv_a_4_1',
            'node-dc-iv_a_4_2' => 'node-dc-iv_a_4_2',
            'node-dc-iv_a_5_1' => 'node-dc-iv_a_5_1',
            'node-dc-iv_a_8_1' => 'node-dc-iv_a_8_1',
            'node-dc-iv_b_1_1' => 'node-dc-iv_b_1_1',
            'node-dc-iv_b_1_2' => 'node-dc-iv_b_1_2',
            'node-dc-iv_b_2_1' => 'node-dc-iv_b_2_1',
            'node-dc-iv_b_2_2' => 'node-dc-iv_b_2_2',
            'node-dc-iv_b_3_1' => 'node-dc-iv_b_3_1',
            'node-dc-iv_b_3_2' => 'node-dc-iv_b_3_2',
            'node-dc-iv_b_3_3' => 'node-dc-iv_b_3_3',
            'node-dc-iv_b_4_1' => 'node-dc-iv_b_4_1',
            'node-dc-iv_b_5_1' => 'node-dc-iv_b_5_1',
            'node-dc-iv_b_6_1' => 'node-dc-iv_b_6_1',
            'node-dc-iv_b_6_2' => 'node-dc-iv_b_6_2',
            'node-dc-iv_b_6_3' => 'node-dc-iv_b_6_3',
            'node-dc-iv_b_6_4' => 'node-dc-iv_b_6_4',
            'node-dc-iv_b_7_1' => 'node-dc-iv_b_7_1',
            'node-dc-iv_b_7_2' => 'node-dc-iv_b_7_2',
            'node-dc-iv_b_7_3' => 'node-dc-iv_b_7_3',
            'node-dc-iv_b_7_4' => 'node-dc-iv_b_7_4',
            'node-dc-iv_b_7_5' => 'node-dc-iv_b_7_5',
            'node-dc-iv_b_7_6' => 'node-dc-iv_b_7_6',
            'node-dc-iv_b_8_1' => 'node-dc-iv_b_8_1',
            'node-dc-iv_b_8_2' => 'node-dc-iv_b_8_2',
            'node-dc-iv_b_8_3' => 'node-dc-iv_b_8_3',
            'node-dc-iv_b_8_4' => 'node-dc-iv_b_8_4',
            'node-dc-iv_b_8_5' => 'node-dc-iv_b_8_5',
            'node-dc-iv_b_8_6' => 'node-dc-iv_b_8_6',
            'node-dc-iv_b_8_7' => 'node-dc-iv_b_8_7',
            'node-dc-iv_b_8_8' => 'node-dc-iv_b_8_8',
            'node-dc-iv_b_8_9' => 'node-dc-iv_b_8_9',
            'node-dc-iv_b_8_10' => 'node-dc-iv_b_8_10',
            'node-dc-iv_b_9_1' => 'node-dc-iv_b_9_1',
            'node-dc-iv_b_9_2' => 'node-dc-iv_b_9_2',
            'node-dc-iv_b_10_1' => 'node-dc-iv_b_10_1',
            'node-dc-iv_b_11_1' => 'node-dc-iv_b_11_1',
            'node-dc-iv_b_11_2' => 'node-dc-iv_b_11_2',
            'node-dc-iv_b_11_3' => 'node-dc-iv_b_11_3',
            'node-dc-iv_b_12_1' => 'node-dc-iv_b_12_1',
            'node-dc-iv_b_12_3' => 'node-dc-iv_b_12_3',
            'node-dc-iv_b_13_1' => 'node-dc-iv_b_13_1',
            'node-dc-iv_c_1_1' => 'node-dc-iv_c_1_1',
            'node-dc-iv_c_2_1' => 'node-dc-iv_c_2_1',
            'node-dc-iv_c_3_1' => 'node-dc-iv_c_3_1',
            'node-dc-iv_c_3_2' => 'node-dc-iv_c_3_2',
            'node-dc-iv_c_4_1' => 'node-dc-iv_c_4_1',
            'node-dc-iv_c_4_2' => 'node-dc-iv_c_4_2',
            'node-dc-iv_c_5_1' => 'node-dc-iv_c_5_1',
            'node-dc-field_testfield' => 'node-dc-field_testfield',
            'node-dc-field_iv_a_5_1_new' => 'node-dc-field_iv_a_5_1_new',
            'node-dc-field_iv_a_6_1' => 'node-dc-field_iv_a_6_1',
            'node-dc-field_year_ceased_operating' => 'node-dc-field_year_ceased_operating',
            'node-dc-field_geolocation' => 'node-dc-field_geolocation',
            'node-dc-field_iv_b_1_2' => 'node-dc-field_iv_b_1_2',
            'node-dc-field_custodial_authority' => 'node-dc-field_custodial_authority',
            'node-dc-field_country' => 'node-dc-field_country',
            'node-dc-field_iv_b_1_1' => 'node-dc-field_iv_b_1_1',
            'node-dc-field_iv_c_2_1' => 'node-dc-field_iv_c_2_1',
          ),
          'dependencies' => 
          array (
            'country' => 'country',
          ),
          'field_base' => 
          array (
            'body' => 'body',
          ),
        ),
        'project path' => 'profiles/gdp_profile_v1_0/modules/custom',
      ),
      'project' => 'gdp_dc',
      'version' => '7.x-1.0',
    ),
    'gdp_fields' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/custom/gdp_fields/gdp_fields/gdp_fields.module',
      'name' => 'gdp_fields',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'GDP Fields',
        'description' => 'TODO: Description of module',
        'package' => 'gdp',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'gdp_fields.module',
          1 => 'gdp_fields.fields.inc',
          2 => 'gdp_fields.install',
          3 => 'gdp_fields/views/gdp_fields.views.inc',
        ),
        'dependencies' => 
        array (
          0 => 'list',
          1 => 'select_or_other',
          2 => 'views',
          3 => 'argfilters',
          4 => 'date',
        ),
        'version' => NULL,
        'php' => '5.2.4',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'gdp_glue' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/custom/gdp_glue/gdp_glue.module',
      'name' => 'gdp_glue',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'GDP Glue',
        'description' => 'Glue code for GDP.',
        'package' => 'Other',
        'core' => '7.x',
        'dependencies' => 
        array (
        ),
        'version' => NULL,
        'php' => '5.2.4',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'gdp_migrate' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/custom/gdp_migrate/gdp_migrate.module',
      'name' => 'gdp_migrate',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'GDP Migration module',
        'description' => 'Module to migrate GDP site content to Drupal 7',
        'package' => 'Development',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'migrate',
          1 => 'field',
          2 => 'file',
          3 => 'image',
          4 => 'number',
          5 => 'text',
          6 => 'pathauto',
        ),
        'files' => 
        array (
          0 => 'gdp_migrate.inc',
        ),
        'version' => NULL,
        'php' => '5.2.4',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'gdp_profile_v1_0' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/gdp_profile_v1_0.profile',
      'name' => 'gdp_profile_v1_0',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '1000',
      'info' => 
      array (
        'name' => 'GDP Profile V1.0',
        'description' => '',
        'exclusive' => '1',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'admin',
          1 => 'admin_devel',
          2 => 'admin_menu',
          3 => 'admin_menu_toolbar',
          4 => 'ctools',
          5 => 'block',
          6 => 'color',
          7 => 'comment',
          8 => 'contextual',
          9 => 'dashboard',
          10 => 'dblog',
          11 => 'field',
          12 => 'field_sql_storage',
          13 => 'field_ui',
          14 => 'file',
          15 => 'filter',
          16 => 'help',
          17 => 'image',
          18 => 'list',
          19 => 'menu',
          20 => 'node',
          21 => 'number',
          22 => 'options',
          23 => 'overlay',
          24 => 'path',
          25 => 'rdf',
          26 => 'search',
          27 => 'shortcut',
          28 => 'system',
          29 => 'taxonomy',
          30 => 'text',
          31 => 'update',
          32 => 'user',
          33 => 'date',
          34 => 'date_api',
          35 => 'ds',
          36 => 'argfilters',
          37 => 'entityreference',
          38 => 'entityreference_behavior_example',
          39 => 'field_group',
          40 => 'link',
          41 => 'better_formats',
          42 => 'diff',
          43 => 'entity',
          44 => 'entity_token',
          45 => 'libraries',
          46 => 'module_filter',
          47 => 'rel',
          48 => 'select_or_other',
          49 => 'strongarm',
          50 => 'token',
          51 => 'views',
          52 => 'views_ui',
          53 => 'gdp_fields',
          54 => 'features',
          55 => 'country',
          56 => 'detention_centre',
          57 => 'sub_region',
          58 => 'treaty',
        ),
        'version' => NULL,
        'php' => '5.2.4',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'geocoder' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/geocoder/geocoder.module',
      'name' => 'geocoder',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Geocoder',
        'description' => 'An API and widget to geocode various known data into other GIS data types.',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'geophp',
          1 => 'ctools',
        ),
        'version' => '7.x-1.2',
        'project' => 'geocoder',
        'datestamp' => '1346083034',
        'php' => '5.2.4',
      ),
      'project' => 'geocoder',
      'version' => '7.x-1.2',
    ),
    'geofield' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/geofield/geofield.module',
      'name' => 'geofield',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Geofield',
        'description' => 'Stores geographic and location data (points, lines, and polygons).',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'geophp',
        ),
        'package' => 'Fields',
        'files' => 
        array (
          0 => 'geofield.module',
          1 => 'geofield.install',
          2 => 'geofield.widgets.inc',
          3 => 'geofield.formatters.inc',
          4 => 'geofield.openlayers.inc',
          5 => 'geofield.feeds.inc',
          6 => 'geofield.test',
        ),
        'version' => '7.x-1.1',
        'project' => 'geofield',
        'datestamp' => '1338941478',
        'php' => '5.2.4',
      ),
      'project' => 'geofield',
      'version' => '7.x-1.1',
    ),
    'geofield_gmap' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/geofield_gmap/geofield_gmap.module',
      'name' => 'geofield_gmap',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Geofield Gmap',
        'description' => 'Google Map widget and formatter for geofield.',
        'dependencies' => 
        array (
          0 => 'geofield',
        ),
        'core' => '7.x',
        'files' => 
        array (
          0 => 'geofield_gmap.admin.inc',
          1 => 'geofield_gmap.install',
          2 => 'geofield_gmap.module',
        ),
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'geofield_gmap.css',
          ),
        ),
        'scripts' => 
        array (
          0 => 'geofield_gmap.js',
        ),
        'configure' => 'admin/config/content/geofield_gmap',
        'version' => '7.x-1.x-dev',
        'project' => 'geofield_gmap',
        'datestamp' => '1354928447',
        'php' => '5.2.4',
      ),
      'project' => 'geofield_gmap',
      'version' => '7.x-1.x-dev',
    ),
    'geofield_map' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/geofield/modules/geofield_map/geofield_map.module',
      'name' => 'geofield_map',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Geofield Map',
        'description' => 'Provides a basic mapping interface for Geofield.',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'geofield',
        ),
        'files' => 
        array (
          0 => 'includes/geofield_map.views.inc',
          1 => 'includes/geofield_map_plugin_style_map.inc',
        ),
        'version' => '7.x-1.1',
        'project' => 'geofield',
        'datestamp' => '1338941478',
        'php' => '5.2.4',
      ),
      'project' => 'geofield',
      'version' => '7.x-1.1',
    ),
    'geophp' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/geophp/geophp.module',
      'name' => 'geophp',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'geoPHP',
        'description' => 'Wraps the geoPHP library: advanced geometry operations in PHP',
        'core' => '7.x',
        'version' => '7.x-1.7',
        'project' => 'geophp',
        'datestamp' => '1352084822',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'geophp',
      'version' => '7.x-1.7',
    ),
    'globalredirect' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/globalredirect/globalredirect.module',
      'name' => 'globalredirect',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => '6101',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Global Redirect',
        'description' => 'Searches for an alias of the current URL and 301 redirects if found. Stops duplicate content arising when path module is enabled.',
        'package' => 'Path management',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'globalredirect.test',
        ),
        'configure' => 'admin/config/system/globalredirect',
        'version' => '7.x-1.5',
        'project' => 'globalredirect',
        'datestamp' => '1339748779',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'globalredirect',
      'version' => '7.x-1.5',
    ),
    'help' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/help/help.module',
      'name' => 'help',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Help',
        'description' => 'Manages the display of online help.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'help.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'image' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/image/image.module',
      'name' => 'image',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7004',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Image',
        'description' => 'Provides image manipulation tools.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'file',
        ),
        'files' => 
        array (
          0 => 'image.test',
        ),
        'configure' => 'admin/config/media/image-styles',
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'image_module_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/image/tests/image_module_test.module',
      'name' => 'image_module_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Image test',
        'description' => 'Provides hook implementations for testing Image module functionality.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'image_module_test.module',
        ),
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'image_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/image_test.module',
      'name' => 'image_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Image test',
        'description' => 'Support module for image toolkit tests.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'inline_entity_form' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/inline_entity_form/inline_entity_form.module',
      'name' => 'inline_entity_form',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Inline Entity Form',
        'description' => 'Provides a widget for inline management (creation, modification, removal) of referenced entities. ',
        'package' => 'Fields',
        'dependencies' => 
        array (
          0 => 'entity',
          1 => 'system (>7.14)',
        ),
        'core' => '7.x',
        'files' => 
        array (
          0 => 'includes/entity.inline_entity_form.inc',
          1 => 'includes/node.inline_entity_form.inc',
          2 => 'includes/taxonomy_term.inline_entity_form.inc',
          3 => 'includes/commerce_product.inline_entity_form.inc',
          4 => 'includes/commerce_line_item.inline_entity_form.inc',
        ),
        'version' => '7.x-1.2',
        'project' => 'inline_entity_form',
        'datestamp' => '1368798850',
        'php' => '5.2.4',
      ),
      'project' => 'inline_entity_form',
      'version' => '7.x-1.2',
    ),
    'job_scheduler' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/job_scheduler/job_scheduler.module',
      'name' => 'job_scheduler',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7101',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Job Scheduler',
        'description' => 'Scheduler API',
        'files' => 
        array (
          0 => 'job_scheduler.module',
          1 => 'job_scheduler.install',
          2 => 'JobScheduler.inc',
          3 => 'JobSchedulerCronTab.inc',
        ),
        'core' => '7.x',
        'php' => '5.2',
        'version' => '7.x-2.0-alpha3',
        'project' => 'job_scheduler',
        'datestamp' => '1336466457',
        'dependencies' => 
        array (
        ),
      ),
      'project' => 'job_scheduler',
      'version' => '7.x-2.0-alpha3',
    ),
    'job_scheduler_trigger' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/job_scheduler/modules/job_scheduler_trigger/job_scheduler_trigger.module',
      'name' => 'job_scheduler_trigger',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Job Scheduler Trigger',
        'description' => 'Creates scheduler triggers that fire up at certain days, times',
        'core' => '7.x',
        'php' => '5.2',
        'dependencies' => 
        array (
          0 => 'job_scheduler',
        ),
        'version' => '7.x-2.0-alpha3',
        'project' => 'job_scheduler',
        'datestamp' => '1336466457',
      ),
      'project' => 'job_scheduler',
      'version' => '7.x-2.0-alpha3',
    ),
    'libraries' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/libraries/libraries.module',
      'name' => 'libraries',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7200',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Libraries',
        'description' => 'Allows version-dependent and shared usage of external libraries.',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'tests/libraries.test',
        ),
        'version' => '7.x-2.1',
        'project' => 'libraries',
        'datestamp' => '1362848412',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'libraries',
      'version' => '7.x-2.1',
    ),
    'libraries_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/libraries/tests/libraries_test.module',
      'name' => 'libraries_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Libraries test module',
        'description' => 'Tests library detection and loading.',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'libraries',
        ),
        'hidden' => true,
        'version' => '7.x-2.1',
        'project' => 'libraries',
        'datestamp' => '1362848412',
        'php' => '5.2.4',
      ),
      'project' => 'libraries',
      'version' => '7.x-2.1',
    ),
    'link' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/link/link.module',
      'name' => 'link',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7001',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Link',
        'description' => 'Defines simple link field types.',
        'core' => '7.x',
        'package' => 'Fields',
        'files' => 
        array (
          0 => 'link.module',
          1 => 'link.migrate.inc',
          2 => 'tests/link.test',
          3 => 'tests/link.attribute.test',
          4 => 'tests/link.crud.test',
          5 => 'tests/link.crud_browser.test',
          6 => 'tests/link.token.test',
          7 => 'tests/link.validate.test',
          8 => 'views/link_views_handler_argument_target.inc',
          9 => 'views/link_views_handler_filter_protocol.inc',
        ),
        'version' => '7.x-1.1',
        'project' => 'link',
        'datestamp' => '1360444361',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'link',
      'version' => '7.x-1.1',
    ),
    'list' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/field/modules/list/list.module',
      'name' => 'list',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7002',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'List',
        'description' => 'Defines list field types. Use with Options to create selection lists.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'field',
          1 => 'options',
        ),
        'files' => 
        array (
          0 => 'tests/list.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'list_centres' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/custom/list_centres/list_centres.module',
      'name' => 'list_centres',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'list-centres',
        'description' => 'view of centres',
        'core' => '7.x',
        'package' => 'Features',
        'php' => '5.2.4',
        'dependencies' => 
        array (
          0 => 'ctools',
          1 => 'features',
          2 => 'field_group',
          3 => 'menu',
        ),
        'features' => 
        array (
          'features_api' => 
          array (
            0 => 'api:1',
          ),
          'menu_custom' => 
          array (
            0 => 'main-menu',
          ),
        ),
        'project path' => 'profiles/gdp_profile_v1_0/modules/custom',
        'version' => NULL,
      ),
      'project' => '',
      'version' => NULL,
    ),
    'list_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/field/modules/list/tests/list_test.module',
      'name' => 'list_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'List test',
        'description' => 'Support module for the List module tests.',
        'core' => '7.x',
        'package' => 'Testing',
        'version' => '7.22',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'locale' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/locale/locale.module',
      'name' => 'locale',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Locale',
        'description' => 'Adds language handling functionality and enables the translation of the user interface to languages other than English.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'locale.test',
        ),
        'configure' => 'admin/config/regional/language',
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'locale_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/locale/tests/locale_test.module',
      'name' => 'locale_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Locale Test',
        'description' => 'Support module for the locale layer tests.',
        'core' => '7.x',
        'package' => 'Testing',
        'version' => '7.22',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'menu' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/menu/menu.module',
      'name' => 'menu',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7003',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Menu',
        'description' => 'Allows administrators to customize the site navigation menu.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'menu.test',
        ),
        'configure' => 'admin/structure/menu',
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'menu_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/menu_test.module',
      'name' => 'menu_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Hook menu tests',
        'description' => 'Support module for menu hook testing.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'menu_token' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/menu_token/menu_token.module',
      'name' => 'menu_token',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => '7004',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Menu Token',
        'description' => 'Provides tokens in menu items (links).',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'menu',
          1 => 'entity',
          2 => 'token',
          3 => 'ctools',
        ),
        'files' => 
        array (
          0 => 'menu_token.test',
        ),
        'configure' => 'admin/config/menu_token',
        'version' => '7.x-1.0-beta4',
        'project' => 'menu_token',
        'datestamp' => '1359157010',
        'php' => '5.2.4',
      ),
      'project' => 'menu_token',
      'version' => '7.x-1.0-beta4',
    ),
    'migrate' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/migrate/migrate.module',
      'name' => 'migrate',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Migrate',
        'description' => 'Import content from external sources',
        'package' => 'Development',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'includes/base.inc',
          1 => 'includes/field_mapping.inc',
          2 => 'includes/migration.inc',
          3 => 'includes/destination.inc',
          4 => 'includes/exception.inc',
          5 => 'includes/group.inc',
          6 => 'includes/handler.inc',
          7 => 'includes/map.inc',
          8 => 'includes/source.inc',
          9 => 'includes/team.inc',
          10 => 'migrate.mail.inc',
          11 => 'plugins/destinations/entity.inc',
          12 => 'plugins/destinations/term.inc',
          13 => 'plugins/destinations/user.inc',
          14 => 'plugins/destinations/node.inc',
          15 => 'plugins/destinations/comment.inc',
          16 => 'plugins/destinations/file.inc',
          17 => 'plugins/destinations/path.inc',
          18 => 'plugins/destinations/fields.inc',
          19 => 'plugins/destinations/poll.inc',
          20 => 'plugins/destinations/table.inc',
          21 => 'plugins/destinations/table_copy.inc',
          22 => 'plugins/destinations/menu.inc',
          23 => 'plugins/destinations/menu_links.inc',
          24 => 'plugins/destinations/statistics.inc',
          25 => 'plugins/sources/csv.inc',
          26 => 'plugins/sources/files.inc',
          27 => 'plugins/sources/json.inc',
          28 => 'plugins/sources/list.inc',
          29 => 'plugins/sources/multiitems.inc',
          30 => 'plugins/sources/sql.inc',
          31 => 'plugins/sources/sqlmap.inc',
          32 => 'plugins/sources/mssql.inc',
          33 => 'plugins/sources/oracle.inc',
          34 => 'plugins/sources/xml.inc',
          35 => 'tests/import/options.test',
          36 => 'tests/plugins/destinations/comment.test',
          37 => 'tests/plugins/destinations/node.test',
          38 => 'tests/plugins/destinations/table.test',
          39 => 'tests/plugins/destinations/term.test',
          40 => 'tests/plugins/destinations/user.test',
          41 => 'tests/plugins/sources/xml.test',
        ),
        'version' => '7.x-2.5',
        'project' => 'migrate',
        'datestamp' => '1352299007',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'migrate',
      'version' => '7.x-2.5',
    ),
    'migrate_example' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/migrate/migrate_example/migrate_example.module',
      'name' => 'migrate_example',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Migrate Example',
        'description' => 'Example migration data.',
        'package' => 'Development',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'taxonomy',
          1 => 'image',
          2 => 'comment',
          3 => 'migrate',
          4 => 'list',
          5 => 'number',
        ),
        'files' => 
        array (
          0 => 'migrate_example.module',
          1 => 'beer.inc',
          2 => 'wine.inc',
        ),
        'version' => '7.x-2.5',
        'project' => 'migrate',
        'datestamp' => '1352299007',
        'php' => '5.2.4',
      ),
      'project' => 'migrate',
      'version' => '7.x-2.5',
    ),
    'migrate_example_baseball' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/migrate/migrate_example_baseball/migrate_example_baseball.module',
      'name' => 'migrate_example_baseball',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'features',
          1 => 'migrate',
          2 => 'number',
        ),
        'description' => 'Import baseball box scores.',
        'features' => 
        array (
          'field' => 
          array (
            0 => 'node-migrate_example_baseball-body',
            1 => 'node-migrate_example_baseball-field_attendance',
            2 => 'node-migrate_example_baseball-field_duration',
            3 => 'node-migrate_example_baseball-field_home_batters',
            4 => 'node-migrate_example_baseball-field_home_game_number',
            5 => 'node-migrate_example_baseball-field_home_pitcher',
            6 => 'node-migrate_example_baseball-field_home_score',
            7 => 'node-migrate_example_baseball-field_home_team',
            8 => 'node-migrate_example_baseball-field_outs',
            9 => 'node-migrate_example_baseball-field_park',
            10 => 'node-migrate_example_baseball-field_start_date',
            11 => 'node-migrate_example_baseball-field_visiting_batters',
            12 => 'node-migrate_example_baseball-field_visiting_pitcher',
            13 => 'node-migrate_example_baseball-field_visiting_score',
            14 => 'node-migrate_example_baseball-field_visiting_team',
          ),
          'node' => 
          array (
            0 => 'migrate_example_baseball',
          ),
        ),
        'files' => 
        array (
          0 => 'migrate_example_baseball.migrate.inc',
        ),
        'name' => 'migrate_example_baseball',
        'package' => 'Migrate Examples',
        'php' => '5.2.4',
        'version' => '7.x-2.5',
        'project' => 'migrate',
        'datestamp' => '1352299007',
      ),
      'project' => 'migrate',
      'version' => '7.x-2.5',
    ),
    'migrate_example_oracle' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/migrate/migrate_example/migrate_example_oracle/migrate_example_oracle.module',
      'name' => 'migrate_example_oracle',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'features',
          1 => 'image',
          2 => 'migrate',
        ),
        'description' => 'Content type supporting example of Oracle migration',
        'features' => 
        array (
          'field' => 
          array (
            0 => 'node-migrate_example_oracle-body',
            1 => 'node-migrate_example_oracle-field_mainimage',
          ),
          'node' => 
          array (
            0 => 'migrate_example_oracle',
          ),
        ),
        'files' => 
        array (
          0 => 'migrate_example_oracle.migrate.inc',
        ),
        'name' => 'Migrate example - Oracle',
        'package' => 'Migrate Examples',
        'project' => 'migrate',
        'version' => '7.x-2.5',
        'datestamp' => '1352299007',
        'php' => '5.2.4',
      ),
      'project' => 'migrate',
      'version' => '7.x-2.5',
    ),
    'migrate_extras' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/migrate_extras/migrate_extras.module',
      'name' => 'migrate_extras',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Migrate Extras',
        'description' => 'Adds migrate module integration with contrib modules and other miscellaneous tweaks.',
        'core' => '7.x',
        'package' => 'Development',
        'dependencies' => 
        array (
          0 => 'migrate',
        ),
        'files' => 
        array (
          0 => 'addressfield.inc',
          1 => 'cck_phone.inc',
          2 => 'entity_api.inc',
          3 => 'flag.inc',
          4 => 'geofield.inc',
          5 => 'interval.inc',
          6 => 'media.inc',
          7 => 'name.inc',
          8 => 'pathauto.inc',
          9 => 'privatemsg.inc',
          10 => 'profile2.inc',
          11 => 'rules.inc',
          12 => 'user_relationships.inc',
          13 => 'votingapi.inc',
          14 => 'webform.inc',
          15 => 'tests/pathauto.test',
        ),
        'version' => '7.x-2.5',
        'project' => 'migrate_extras',
        'datestamp' => '1352299013',
        'php' => '5.2.4',
      ),
      'project' => 'migrate_extras',
      'version' => '7.x-2.5',
    ),
    'migrate_extras_media' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/migrate_extras/migrate_extras_examples/migrate_extras_media/migrate_extras_media.module',
      'name' => 'migrate_extras_media',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'features',
          1 => 'file',
          2 => 'media',
          3 => 'media_youtube',
          4 => 'migrate',
          5 => 'migrate_extras',
        ),
        'description' => 'Examples for migrating Media',
        'features' => 
        array (
          'field' => 
          array (
            0 => 'node-migrate_extras_media_example-body',
            1 => 'node-migrate_extras_media_example-field_document',
            2 => 'node-migrate_extras_media_example-field_media_image',
            3 => 'node-migrate_extras_media_example-field_youtube_video',
          ),
          'node' => 
          array (
            0 => 'migrate_extras_media_example',
          ),
        ),
        'files' => 
        array (
          0 => 'migrate_extras_media.migrate.inc',
        ),
        'name' => 'Migrate Extras Media',
        'package' => 'Migrate Examples',
        'version' => '7.x-2.5',
        'project' => 'migrate_extras',
        'datestamp' => '1352299013',
        'php' => '5.2.4',
      ),
      'project' => 'migrate_extras',
      'version' => '7.x-2.5',
    ),
    'migrate_extras_pathauto' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/migrate_extras/migrate_extras_examples/migrate_extras_pathauto/migrate_extras_pathauto.module',
      'name' => 'migrate_extras_pathauto',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'features',
          1 => 'migrate_extras',
          2 => 'pathauto',
        ),
        'description' => 'Examples of migrating with the Pathauto module',
        'features' => 
        array (
          'field' => 
          array (
            0 => 'node-migrate_example_pathauto-body',
          ),
          'node' => 
          array (
            0 => 'migrate_example_pathauto',
          ),
        ),
        'files' => 
        array (
          0 => 'migrate_extras_pathauto.migrate.inc',
        ),
        'name' => 'Migrate Extras Pathauto Example',
        'package' => 'Migrate Examples',
        'project' => 'migrate_extras',
        'version' => '7.x-2.5',
        'datestamp' => '1352299013',
        'php' => '5.2.4',
      ),
      'project' => 'migrate_extras',
      'version' => '7.x-2.5',
    ),
    'migrate_extras_profile2' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/migrate_extras/migrate_extras_examples/migrate_extras_profile2/migrate_extras_profile2.module',
      'name' => 'migrate_extras_profile2',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'migrate_extras',
          1 => 'profile2',
        ),
        'description' => 'Examples of migrating into Profile2 entities',
        'files' => 
        array (
          0 => 'migrate_extras_profile2.migrate.inc',
        ),
        'name' => 'Migrate Extras Profile2 Example',
        'package' => 'Migrate Examples',
        'version' => '7.x-2.5',
        'project' => 'migrate_extras',
        'datestamp' => '1352299013',
        'php' => '5.2.4',
      ),
      'project' => 'migrate_extras',
      'version' => '7.x-2.5',
    ),
    'migrate_ui' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/migrate/migrate_ui/migrate_ui.module',
      'name' => 'migrate_ui',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Migrate UI',
        'description' => 'UI for managing migration processes',
        'package' => 'Development',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'migrate',
        ),
        'files' => 
        array (
          0 => 'migrate_ui.module',
        ),
        'version' => '7.x-2.5',
        'project' => 'migrate',
        'datestamp' => '1352299007',
        'php' => '5.2.4',
      ),
      'project' => 'migrate',
      'version' => '7.x-2.5',
    ),
    'module_filter' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/module_filter/module_filter.module',
      'name' => 'module_filter',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7100',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Module filter',
        'description' => 'Filter the modules list.',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'module_filter.install',
          1 => 'module_filter.js',
          2 => 'module_filter.module',
          3 => 'module_filter.admin.inc',
          4 => 'module_filter.theme.inc',
          5 => 'css/module_filter.css',
          6 => 'css/module_filter_tab.css',
          7 => 'js/module_filter.js',
          8 => 'js/module_filter_tab.js',
        ),
        'configure' => 'admin/config/user-interface/modulefilter',
        'version' => '7.x-1.7',
        'project' => 'module_filter',
        'datestamp' => '1341518501',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'module_filter',
      'version' => '7.x-1.7',
    ),
    'module_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/module_test.module',
      'name' => 'module_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Module test',
        'description' => 'Support module for module system testing.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'node' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/node/node.module',
      'name' => 'node',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7013',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Node',
        'description' => 'Allows content to be submitted to the site and displayed on pages.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'node.module',
          1 => 'node.test',
        ),
        'required' => true,
        'configure' => 'admin/structure/types',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'node.css',
          ),
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'node_access_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/node/tests/node_access_test.module',
      'name' => 'node_access_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Node module access tests',
        'description' => 'Support module for node permission testing.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'node_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/node/tests/node_test.module',
      'name' => 'node_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Node module tests',
        'description' => 'Support module for node related testing.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'node_test_exception' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/node/tests/node_test_exception.module',
      'name' => 'node_test_exception',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Node module exception tests',
        'description' => 'Support module for node related exception testing.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'number' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/field/modules/number/number.module',
      'name' => 'number',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Number',
        'description' => 'Defines numeric field types.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'field',
        ),
        'files' => 
        array (
          0 => 'number.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'openid' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/openid/openid.module',
      'name' => 'openid',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'OpenID',
        'description' => 'Allows users to log into your site using OpenID.',
        'version' => '7.22',
        'package' => 'Core',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'openid.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'openid_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/openid/tests/openid_test.module',
      'name' => 'openid_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'OpenID dummy provider',
        'description' => 'OpenID provider used for testing.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'openid',
        ),
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'options' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/field/modules/options/options.module',
      'name' => 'options',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Options',
        'description' => 'Defines selection, check box and radio button widgets for text and numeric fields.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'field',
        ),
        'files' => 
        array (
          0 => 'options.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'overlay' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/overlay/overlay.module',
      'name' => 'overlay',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '1',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Overlay',
        'description' => 'Displays the Drupal administration interface in an overlay.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'page_manager' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/ctools/page_manager/page_manager.module',
      'name' => 'page_manager',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Page manager',
        'description' => 'Provides a UI and API to manage pages within the site.',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'package' => 'Chaos tool suite',
        'version' => '7.x-1.3',
        'project' => 'ctools',
        'datestamp' => '1365013512',
        'php' => '5.2.4',
      ),
      'project' => 'ctools',
      'version' => '7.x-1.3',
    ),
    'path' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/path/path.module',
      'name' => 'path',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Path',
        'description' => 'Allows users to rename URLs.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'path.test',
        ),
        'configure' => 'admin/config/search/path',
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'pathauto' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/pathauto/pathauto.module',
      'name' => 'pathauto',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7005',
      'weight' => '1',
      'info' => 
      array (
        'name' => 'Pathauto',
        'description' => 'Provides a mechanism for modules to automatically generate aliases for the content they manage.',
        'dependencies' => 
        array (
          0 => 'path',
          1 => 'token',
        ),
        'core' => '7.x',
        'files' => 
        array (
          0 => 'pathauto.test',
        ),
        'configure' => 'admin/config/search/path/patterns',
        'recommends' => 
        array (
          0 => 'redirect',
        ),
        'version' => '7.x-1.2',
        'project' => 'pathauto',
        'datestamp' => '1344525185',
        'php' => '5.2.4',
      ),
      'project' => 'pathauto',
      'version' => '7.x-1.2',
    ),
    'path_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/path_test.module',
      'name' => 'path_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Hook path tests',
        'description' => 'Support module for path hook testing.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'php' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/php/php.module',
      'name' => 'php',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'PHP filter',
        'description' => 'Allows embedded PHP code/snippets to be evaluated.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'php.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'poll' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/poll/poll.module',
      'name' => 'poll',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Poll',
        'description' => 'Allows your site to capture votes on different topics in the form of multiple choice questions.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'poll.test',
        ),
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'poll.css',
          ),
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'profile' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/profile/profile.module',
      'name' => 'profile',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Profile',
        'description' => 'Supports configurable user profiles.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'profile.test',
        ),
        'configure' => 'admin/config/people/profile',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'profiler_builder' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/profiler_builder/profiler_builder.module',
      'name' => 'profiler_builder',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Profiler Builder',
        'description' => 'Turn this site into a profiler distribution fast!',
        'package' => 'Development',
        'core' => '7.x',
        'configure' => 'admin/config/development/profiler_builder',
        'version' => '7.x-1.0-rc4',
        'project' => 'profiler_builder',
        'datestamp' => '1364330717',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'profiler_builder',
      'version' => '7.x-1.0-rc4',
    ),
    'psr_0_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/psr_0_test/psr_0_test.module',
      'name' => 'psr_0_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'PSR-0 Test cases',
        'description' => 'Test classes to be discovered by simpletest.',
        'core' => '7.x',
        'hidden' => true,
        'package' => 'Testing',
        'version' => '7.22',
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'quicktabs' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/quicktabs/quicktabs.module',
      'name' => 'quicktabs',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Quicktabs',
        'description' => 'Render content with tabs and other display styles',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'quicktabs.module',
          1 => 'quicktabs.classes.inc',
          2 => 'includes/quicktabs_style_plugin.inc',
          3 => 'tests/quicktabs.test',
        ),
        'configure' => 'admin/structure/quicktabs',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'version' => '7.x-3.4',
        'project' => 'quicktabs',
        'datestamp' => '1332980461',
        'php' => '5.2.4',
      ),
      'project' => 'quicktabs',
      'version' => '7.x-3.4',
    ),
    'quicktabs_tabstyles' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/quicktabs/quicktabs_tabstyles/quicktabs_tabstyles.module',
      'name' => 'quicktabs_tabstyles',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Quicktabs Styles',
        'description' => 'Adds predefined tab styles to choose from per Quicktabs instance.',
        'core' => '7.x',
        'configure' => 'admin/structure/quicktabs/styles',
        'dependencies' => 
        array (
          0 => 'quicktabs',
        ),
        'version' => '7.x-3.4',
        'project' => 'quicktabs',
        'datestamp' => '1332980461',
        'php' => '5.2.4',
      ),
      'project' => 'quicktabs',
      'version' => '7.x-3.4',
    ),
    'rdf' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/rdf/rdf.module',
      'name' => 'rdf',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'RDF',
        'description' => 'Enriches your content with metadata to let other applications (e.g. search engines, aggregators) better understand its relationships and attributes.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'rdf.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'rdf_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/rdf/tests/rdf_test.module',
      'name' => 'rdf_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'RDF module tests',
        'description' => 'Support module for RDF module testing.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'rel' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/rel/rel.module',
      'name' => 'rel',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => '7002',
      'weight' => '10',
      'info' => 
      array (
        'name' => 'Renderable elements',
        'description' => 'Register any forms to enable you to manage the display through an UI',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'files' => 
        array (
          0 => 'rel.module',
          1 => 'rel.test',
          2 => 'rel.admin.inc',
        ),
        'version' => '7.x-1.0-alpha2',
        'project' => 'rel',
        'datestamp' => '1358839857',
        'php' => '5.2.4',
      ),
      'project' => 'rel',
      'version' => '7.x-1.0-alpha2',
    ),
    'rel_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/rel/tests/rel_test.module',
      'name' => 'rel_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Rendereable elements Test',
        'description' => 'Test module for renderable elements',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'rel',
        ),
        'hidden' => true,
        'version' => '7.x-1.0-alpha2',
        'project' => 'rel',
        'datestamp' => '1358839857',
        'php' => '5.2.4',
      ),
      'project' => 'rel',
      'version' => '7.x-1.0-alpha2',
    ),
    'requirements1_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/requirements1_test.module',
      'name' => 'requirements1_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Requirements 1 Test',
        'description' => 'Tests that a module is not installed when it fails hook_requirements(\'install\').',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'requirements2_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/requirements2_test.module',
      'name' => 'requirements2_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Requirements 2 Test',
        'description' => 'Tests that a module is not installed when the one it depends on fails hook_requirements(\'install).',
        'dependencies' => 
        array (
          0 => 'requirements1_test',
          1 => 'comment',
        ),
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'search' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/search/search.module',
      'name' => 'search',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7000',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Search',
        'description' => 'Enables site-wide keyword searching.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'search.extender.inc',
          1 => 'search.test',
        ),
        'configure' => 'admin/config/search/settings',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'search.css',
          ),
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'search_embedded_form' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/search/tests/search_embedded_form.module',
      'name' => 'search_embedded_form',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Search embedded form',
        'description' => 'Support module for search module testing of embedded forms.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'search_extra_type' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/search/tests/search_extra_type.module',
      'name' => 'search_extra_type',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Test search type',
        'description' => 'Support module for search module testing.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'select_or_other' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/select_or_other/select_or_other.module',
      'name' => 'select_or_other',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Select (or other)',
        'description' => 'Provides a select box form element with additional option \'Other\' to give a textfield.',
        'core' => '7.x',
        'version' => '7.x-2.15',
        'project' => 'select_or_other',
        'datestamp' => '1345436315',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'select_or_other',
      'version' => '7.x-2.15',
    ),
    'session_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/session_test.module',
      'name' => 'session_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Session test',
        'description' => 'Support module for session data testing.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'shortcut' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/shortcut/shortcut.module',
      'name' => 'shortcut',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Shortcut',
        'description' => 'Allows users to manage customizable lists of shortcut links.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'shortcut.test',
        ),
        'configure' => 'admin/config/user-interface/shortcut',
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'simpletest' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/simpletest.module',
      'name' => 'simpletest',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Testing',
        'description' => 'Provides a framework for unit and functional testing.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'simpletest.test',
          1 => 'drupal_web_test_case.php',
          2 => 'tests/actions.test',
          3 => 'tests/ajax.test',
          4 => 'tests/batch.test',
          5 => 'tests/bootstrap.test',
          6 => 'tests/cache.test',
          7 => 'tests/common.test',
          8 => 'tests/database_test.test',
          9 => 'tests/entity_crud_hook_test.test',
          10 => 'tests/entity_query.test',
          11 => 'tests/error.test',
          12 => 'tests/file.test',
          13 => 'tests/filetransfer.test',
          14 => 'tests/form.test',
          15 => 'tests/graph.test',
          16 => 'tests/image.test',
          17 => 'tests/lock.test',
          18 => 'tests/mail.test',
          19 => 'tests/menu.test',
          20 => 'tests/module.test',
          21 => 'tests/pager.test',
          22 => 'tests/password.test',
          23 => 'tests/path.test',
          24 => 'tests/registry.test',
          25 => 'tests/schema.test',
          26 => 'tests/session.test',
          27 => 'tests/tablesort.test',
          28 => 'tests/theme.test',
          29 => 'tests/unicode.test',
          30 => 'tests/update.test',
          31 => 'tests/xmlrpc.test',
          32 => 'tests/upgrade/upgrade.test',
          33 => 'tests/upgrade/upgrade.comment.test',
          34 => 'tests/upgrade/upgrade.filter.test',
          35 => 'tests/upgrade/upgrade.forum.test',
          36 => 'tests/upgrade/upgrade.locale.test',
          37 => 'tests/upgrade/upgrade.menu.test',
          38 => 'tests/upgrade/upgrade.node.test',
          39 => 'tests/upgrade/upgrade.taxonomy.test',
          40 => 'tests/upgrade/upgrade.trigger.test',
          41 => 'tests/upgrade/upgrade.translatable.test',
          42 => 'tests/upgrade/upgrade.upload.test',
          43 => 'tests/upgrade/upgrade.user.test',
          44 => 'tests/upgrade/update.aggregator.test',
          45 => 'tests/upgrade/update.trigger.test',
          46 => 'tests/upgrade/update.field.test',
          47 => 'tests/upgrade/update.user.test',
        ),
        'configure' => 'admin/config/development/testing/settings',
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'statistics' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/statistics/statistics.module',
      'name' => 'statistics',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Statistics',
        'description' => 'Logs access statistics for your site.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'statistics.test',
        ),
        'configure' => 'admin/config/system/statistics',
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'strongarm' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/strongarm/strongarm.module',
      'name' => 'strongarm',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7201',
      'weight' => '-1000',
      'info' => 
      array (
        'name' => 'Strongarm',
        'description' => 'Enforces variable values defined by modules that need settings set to operate properly.',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'files' => 
        array (
          0 => 'strongarm.admin.inc',
          1 => 'strongarm.install',
          2 => 'strongarm.module',
        ),
        'version' => '7.x-2.0',
        'project' => 'strongarm',
        'datestamp' => '1339604214',
        'php' => '5.2.4',
      ),
      'project' => 'strongarm',
      'version' => '7.x-2.0',
    ),
    'stylizer' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/ctools/stylizer/stylizer.module',
      'name' => 'stylizer',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Stylizer',
        'description' => 'Create custom styles for applications such as Panels.',
        'core' => '7.x',
        'package' => 'Chaos tool suite',
        'dependencies' => 
        array (
          0 => 'ctools',
          1 => 'color',
        ),
        'version' => '7.x-1.3',
        'project' => 'ctools',
        'datestamp' => '1365013512',
        'php' => '5.2.4',
      ),
      'project' => 'ctools',
      'version' => '7.x-1.3',
    ),
    'sub_region' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22',
      'name' => 'sub_region',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '5.2.4',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'syslog' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/syslog/syslog.module',
      'name' => 'syslog',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '1',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Syslog',
        'description' => 'Logs and records system events to syslog.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'syslog.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'system' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/system/system.module',
      'name' => 'system',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '1',
      'schema_version' => '7078',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'System',
        'description' => 'Handles general site configuration for administrators.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'system.archiver.inc',
          1 => 'system.mail.inc',
          2 => 'system.queue.inc',
          3 => 'system.tar.inc',
          4 => 'system.updater.inc',
          5 => 'system.test',
        ),
        'required' => true,
        'configure' => 'admin/config/system',
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'system_dependencies_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/system_dependencies_test.module',
      'name' => 'system_dependencies_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'System dependency test',
        'description' => 'Support module for testing system dependencies.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'dependencies' => 
        array (
          0 => '_missing_dependency',
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'system_incompatible_core_version_dependencies_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/system_incompatible_core_version_dependencies_test.module',
      'name' => 'system_incompatible_core_version_dependencies_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'System incompatible core version dependencies test',
        'description' => 'Support module for testing system dependencies.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'dependencies' => 
        array (
          0 => 'system_incompatible_core_version_test',
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'system_incompatible_core_version_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/system_incompatible_core_version_test.module',
      'name' => 'system_incompatible_core_version_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'System incompatible core version test',
        'description' => 'Support module for testing system dependencies.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '5.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'system_incompatible_module_version_dependencies_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/system_incompatible_module_version_dependencies_test.module',
      'name' => 'system_incompatible_module_version_dependencies_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'System incompatible module version dependencies test',
        'description' => 'Support module for testing system dependencies.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'dependencies' => 
        array (
          0 => 'system_incompatible_module_version_test (>2.0)',
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'system_incompatible_module_version_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/system_incompatible_module_version_test.module',
      'name' => 'system_incompatible_module_version_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'System incompatible module version test',
        'description' => 'Support module for testing system dependencies.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'system_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/system_test.module',
      'name' => 'system_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'System test',
        'description' => 'Support module for system testing.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'system_test.module',
        ),
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'tabtamer' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/tabtamer/tabtamer.module',
      'name' => 'tabtamer',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '90',
      'info' => 
      array (
        'name' => 'Tab Tamer',
        'description' => 'Gives additional control over what tabs are seen by a user.',
        'package' => 'Other',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'tabtamer.install',
          1 => 'tabtamer.module',
        ),
        'configure' => 'admin/config/system/tabtamer',
        'version' => '7.x-1.1',
        'project' => 'tabtamer',
        'datestamp' => '1340140644',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'tabtamer',
      'version' => '7.x-1.1',
    ),
    'taxonomy' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/taxonomy/taxonomy.module',
      'name' => 'taxonomy',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7010',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Taxonomy',
        'description' => 'Enables the categorization of content.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'options',
        ),
        'files' => 
        array (
          0 => 'taxonomy.module',
          1 => 'taxonomy.test',
        ),
        'configure' => 'admin/structure/taxonomy',
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'taxonomy_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/taxonomy_test.module',
      'name' => 'taxonomy_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Taxonomy test module',
        'description' => '"Tests functions and hooks not used in core".',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'dependencies' => 
        array (
          0 => 'taxonomy',
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'test_group_fields' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22',
      'name' => 'test_group_fields',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'dependencies' => 
        array (
        ),
        'description' => '',
        'version' => NULL,
        'php' => '5.2.4',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'text' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/field/modules/text/text.module',
      'name' => 'text',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7000',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Text',
        'description' => 'Defines simple text field types.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'field',
        ),
        'files' => 
        array (
          0 => 'text.test',
        ),
        'required' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'theme_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/theme_test.module',
      'name' => 'theme_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Theme test',
        'description' => 'Support module for theme system testing.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'token' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/token/token.module',
      'name' => 'token',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7001',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Token',
        'description' => 'Provides a user interface for the Token API and some missing core tokens.',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'token.test',
        ),
        'version' => '7.x-1.5',
        'project' => 'token',
        'datestamp' => '1361665026',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'token',
      'version' => '7.x-1.5',
    ),
    'token_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/token/tests/token_test.module',
      'name' => 'token_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Token Test',
        'description' => 'Testing module for token functionality.',
        'package' => 'Testing',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'token_test.module',
        ),
        'hidden' => true,
        'version' => '7.x-1.5',
        'project' => 'token',
        'datestamp' => '1361665026',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'token',
      'version' => '7.x-1.5',
    ),
    'toolbar' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/toolbar/toolbar.module',
      'name' => 'toolbar',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Toolbar',
        'description' => 'Provides a toolbar that shows the top-level administration menu items and links from other modules.',
        'core' => '7.x',
        'package' => 'Core',
        'version' => '7.22',
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'tracker' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/tracker/tracker.module',
      'name' => 'tracker',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Tracker',
        'description' => 'Enables tracking of recent content for users.',
        'dependencies' => 
        array (
          0 => 'comment',
        ),
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'tracker.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'translation' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/translation/translation.module',
      'name' => 'translation',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Content translation',
        'description' => 'Allows content to be translated into different languages.',
        'dependencies' => 
        array (
          0 => 'locale',
        ),
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'translation.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'translation_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/translation/tests/translation_test.module',
      'name' => 'translation_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Content Translation Test',
        'description' => 'Support module for the content translation tests.',
        'core' => '7.x',
        'package' => 'Testing',
        'version' => '7.22',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'treaty' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/custom/treaty/treaty.module',
      'name' => 'treaty',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Treaty',
        'core' => '7.x',
        'package' => 'Features',
        'version' => '7.x-1.0',
        'project' => 'treaty',
        'dependencies' => 
        array (
          0 => 'ctools',
          1 => 'date',
          2 => 'entityreference',
          3 => 'features',
          4 => 'options',
          5 => 'strongarm',
          6 => 'text',
        ),
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'strongarm:strongarm:1',
          ),
          'features_api' => 
          array (
            0 => 'api:1',
          ),
          'field_base' => 
          array (
            0 => 'field_treaty_agreement_date',
            1 => 'field_treaty_short_name',
          ),
          'field_instance' => 
          array (
            0 => 'node-treaty-body',
            1 => 'node-treaty-field_gdp_subregion_ref',
            2 => 'node-treaty-field_treaty_agreement_date',
            3 => 'node-treaty-field_treaty_short_name',
          ),
          'node' => 
          array (
            0 => 'treaty',
          ),
          'variable' => 
          array (
            0 => 'comment_anonymous_treaty',
            1 => 'comment_default_mode_treaty',
            2 => 'comment_default_per_page_treaty',
            3 => 'comment_form_location_treaty',
            4 => 'comment_preview_treaty',
            5 => 'comment_subject_field_treaty',
            6 => 'comment_treaty',
            7 => 'field_bundle_settings_node__treaty',
            8 => 'menu_options_treaty',
            9 => 'menu_parent_treaty',
            10 => 'node_options_treaty',
            11 => 'node_preview_treaty',
            12 => 'node_submitted_treaty',
          ),
        ),
        'features_exclude' => 
        array (
          'field_base' => 
          array (
            'body' => 'body',
            'field_gdp_subregion_ref' => 'field_gdp_subregion_ref',
          ),
          'field' => 
          array (
            'node-treaty-body' => 'node-treaty-body',
            'node-treaty-field_treaty_short_name' => 'node-treaty-field_treaty_short_name',
            'node-treaty-field_treaty_agreement_date' => 'node-treaty-field_treaty_agreement_date',
            'node-treaty-field_gdp_subregion_ref' => 'node-treaty-field_gdp_subregion_ref',
          ),
        ),
        'description' => '',
        'php' => '5.2.4',
      ),
      'project' => 'treaty',
      'version' => '7.x-1.0',
    ),
    'trigger' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/trigger/trigger.module',
      'name' => 'trigger',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Trigger',
        'description' => 'Enables actions to be fired on certain system events, such as when new content is created.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'trigger.test',
        ),
        'configure' => 'admin/structure/trigger',
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'trigger_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/trigger/tests/trigger_test.module',
      'name' => 'trigger_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Trigger Test',
        'description' => 'Support module for Trigger tests.',
        'package' => 'Testing',
        'core' => '7.x',
        'hidden' => true,
        'version' => '7.22',
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'update' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/update/update.module',
      'name' => 'update',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7001',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Update manager',
        'description' => 'Checks for available updates, and can securely install or update modules and themes via a web interface.',
        'version' => '7.22',
        'package' => 'Core',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'update.test',
        ),
        'configure' => 'admin/reports/updates/settings',
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'update_script_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/update_script_test.module',
      'name' => 'update_script_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Update script test',
        'description' => 'Support module for update script testing.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'update_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/update/tests/update_test.module',
      'name' => 'update_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Update test',
        'description' => 'Support module for update module testing.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'update_test_1' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/update_test_1.module',
      'name' => 'update_test_1',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Update test',
        'description' => 'Support module for update testing.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'update_test_2' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/update_test_2.module',
      'name' => 'update_test_2',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Update test',
        'description' => 'Support module for update testing.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'update_test_3' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/update_test_3.module',
      'name' => 'update_test_3',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Update test',
        'description' => 'Support module for update testing.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'url_alter_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/url_alter_test.module',
      'name' => 'url_alter_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Url_alter tests',
        'description' => 'A support modules for url_alter hook testing.',
        'core' => '7.x',
        'package' => 'Testing',
        'version' => '7.22',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'user' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/user/user.module',
      'name' => 'user',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7018',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'User',
        'description' => 'Manages the user registration and login system.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'user.module',
          1 => 'user.test',
        ),
        'required' => true,
        'configure' => 'admin/config/people',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'user.css',
          ),
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'user_form_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/user/tests/user_form_test.module',
      'name' => 'user_form_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'User module form tests',
        'description' => 'Support module for user form testing.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'views' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/views/views.module',
      'name' => 'views',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7301',
      'weight' => '10',
      'info' => 
      array (
        'name' => 'Views',
        'description' => 'Create customized lists and queries from your database.',
        'package' => 'Views',
        'core' => '7.x',
        'php' => '5.2',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'css/views.css',
          ),
        ),
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'files' => 
        array (
          0 => 'handlers/views_handler_area.inc',
          1 => 'handlers/views_handler_area_result.inc',
          2 => 'handlers/views_handler_area_text.inc',
          3 => 'handlers/views_handler_area_text_custom.inc',
          4 => 'handlers/views_handler_area_view.inc',
          5 => 'handlers/views_handler_argument.inc',
          6 => 'handlers/views_handler_argument_date.inc',
          7 => 'handlers/views_handler_argument_formula.inc',
          8 => 'handlers/views_handler_argument_many_to_one.inc',
          9 => 'handlers/views_handler_argument_null.inc',
          10 => 'handlers/views_handler_argument_numeric.inc',
          11 => 'handlers/views_handler_argument_string.inc',
          12 => 'handlers/views_handler_argument_group_by_numeric.inc',
          13 => 'handlers/views_handler_field.inc',
          14 => 'handlers/views_handler_field_counter.inc',
          15 => 'handlers/views_handler_field_boolean.inc',
          16 => 'handlers/views_handler_field_contextual_links.inc',
          17 => 'handlers/views_handler_field_custom.inc',
          18 => 'handlers/views_handler_field_date.inc',
          19 => 'handlers/views_handler_field_entity.inc',
          20 => 'handlers/views_handler_field_markup.inc',
          21 => 'handlers/views_handler_field_math.inc',
          22 => 'handlers/views_handler_field_numeric.inc',
          23 => 'handlers/views_handler_field_prerender_list.inc',
          24 => 'handlers/views_handler_field_time_interval.inc',
          25 => 'handlers/views_handler_field_serialized.inc',
          26 => 'handlers/views_handler_field_machine_name.inc',
          27 => 'handlers/views_handler_field_url.inc',
          28 => 'handlers/views_handler_filter.inc',
          29 => 'handlers/views_handler_filter_boolean_operator.inc',
          30 => 'handlers/views_handler_filter_boolean_operator_string.inc',
          31 => 'handlers/views_handler_filter_combine.inc',
          32 => 'handlers/views_handler_filter_date.inc',
          33 => 'handlers/views_handler_filter_equality.inc',
          34 => 'handlers/views_handler_filter_entity_bundle.inc',
          35 => 'handlers/views_handler_filter_group_by_numeric.inc',
          36 => 'handlers/views_handler_filter_in_operator.inc',
          37 => 'handlers/views_handler_filter_many_to_one.inc',
          38 => 'handlers/views_handler_filter_numeric.inc',
          39 => 'handlers/views_handler_filter_string.inc',
          40 => 'handlers/views_handler_relationship.inc',
          41 => 'handlers/views_handler_relationship_groupwise_max.inc',
          42 => 'handlers/views_handler_sort.inc',
          43 => 'handlers/views_handler_sort_date.inc',
          44 => 'handlers/views_handler_sort_formula.inc',
          45 => 'handlers/views_handler_sort_group_by_numeric.inc',
          46 => 'handlers/views_handler_sort_menu_hierarchy.inc',
          47 => 'handlers/views_handler_sort_random.inc',
          48 => 'includes/base.inc',
          49 => 'includes/handlers.inc',
          50 => 'includes/plugins.inc',
          51 => 'includes/view.inc',
          52 => 'modules/aggregator/views_handler_argument_aggregator_fid.inc',
          53 => 'modules/aggregator/views_handler_argument_aggregator_iid.inc',
          54 => 'modules/aggregator/views_handler_argument_aggregator_category_cid.inc',
          55 => 'modules/aggregator/views_handler_field_aggregator_title_link.inc',
          56 => 'modules/aggregator/views_handler_field_aggregator_category.inc',
          57 => 'modules/aggregator/views_handler_field_aggregator_item_description.inc',
          58 => 'modules/aggregator/views_handler_field_aggregator_xss.inc',
          59 => 'modules/aggregator/views_handler_filter_aggregator_category_cid.inc',
          60 => 'modules/aggregator/views_plugin_row_aggregator_rss.inc',
          61 => 'modules/book/views_plugin_argument_default_book_root.inc',
          62 => 'modules/comment/views_handler_argument_comment_user_uid.inc',
          63 => 'modules/comment/views_handler_field_comment.inc',
          64 => 'modules/comment/views_handler_field_comment_depth.inc',
          65 => 'modules/comment/views_handler_field_comment_link.inc',
          66 => 'modules/comment/views_handler_field_comment_link_approve.inc',
          67 => 'modules/comment/views_handler_field_comment_link_delete.inc',
          68 => 'modules/comment/views_handler_field_comment_link_edit.inc',
          69 => 'modules/comment/views_handler_field_comment_link_reply.inc',
          70 => 'modules/comment/views_handler_field_comment_node_link.inc',
          71 => 'modules/comment/views_handler_field_comment_username.inc',
          72 => 'modules/comment/views_handler_field_ncs_last_comment_name.inc',
          73 => 'modules/comment/views_handler_field_ncs_last_updated.inc',
          74 => 'modules/comment/views_handler_field_node_comment.inc',
          75 => 'modules/comment/views_handler_field_node_new_comments.inc',
          76 => 'modules/comment/views_handler_field_last_comment_timestamp.inc',
          77 => 'modules/comment/views_handler_filter_comment_user_uid.inc',
          78 => 'modules/comment/views_handler_filter_ncs_last_updated.inc',
          79 => 'modules/comment/views_handler_filter_node_comment.inc',
          80 => 'modules/comment/views_handler_sort_comment_thread.inc',
          81 => 'modules/comment/views_handler_sort_ncs_last_comment_name.inc',
          82 => 'modules/comment/views_handler_sort_ncs_last_updated.inc',
          83 => 'modules/comment/views_plugin_row_comment_rss.inc',
          84 => 'modules/comment/views_plugin_row_comment_view.inc',
          85 => 'modules/contact/views_handler_field_contact_link.inc',
          86 => 'modules/field/views_handler_field_field.inc',
          87 => 'modules/field/views_handler_relationship_entity_reverse.inc',
          88 => 'modules/field/views_handler_argument_field_list.inc',
          89 => 'modules/field/views_handler_argument_field_list_string.inc',
          90 => 'modules/field/views_handler_filter_field_list.inc',
          91 => 'modules/filter/views_handler_field_filter_format_name.inc',
          92 => 'modules/locale/views_handler_field_node_language.inc',
          93 => 'modules/locale/views_handler_filter_node_language.inc',
          94 => 'modules/locale/views_handler_argument_locale_group.inc',
          95 => 'modules/locale/views_handler_argument_locale_language.inc',
          96 => 'modules/locale/views_handler_field_locale_group.inc',
          97 => 'modules/locale/views_handler_field_locale_language.inc',
          98 => 'modules/locale/views_handler_field_locale_link_edit.inc',
          99 => 'modules/locale/views_handler_filter_locale_group.inc',
          100 => 'modules/locale/views_handler_filter_locale_language.inc',
          101 => 'modules/locale/views_handler_filter_locale_version.inc',
          102 => 'modules/node/views_handler_argument_dates_various.inc',
          103 => 'modules/node/views_handler_argument_node_language.inc',
          104 => 'modules/node/views_handler_argument_node_nid.inc',
          105 => 'modules/node/views_handler_argument_node_type.inc',
          106 => 'modules/node/views_handler_argument_node_vid.inc',
          107 => 'modules/node/views_handler_argument_node_uid_revision.inc',
          108 => 'modules/node/views_handler_field_history_user_timestamp.inc',
          109 => 'modules/node/views_handler_field_node.inc',
          110 => 'modules/node/views_handler_field_node_link.inc',
          111 => 'modules/node/views_handler_field_node_link_delete.inc',
          112 => 'modules/node/views_handler_field_node_link_edit.inc',
          113 => 'modules/node/views_handler_field_node_revision.inc',
          114 => 'modules/node/views_handler_field_node_revision_link.inc',
          115 => 'modules/node/views_handler_field_node_revision_link_delete.inc',
          116 => 'modules/node/views_handler_field_node_revision_link_revert.inc',
          117 => 'modules/node/views_handler_field_node_path.inc',
          118 => 'modules/node/views_handler_field_node_type.inc',
          119 => 'modules/node/views_handler_filter_history_user_timestamp.inc',
          120 => 'modules/node/views_handler_filter_node_access.inc',
          121 => 'modules/node/views_handler_filter_node_status.inc',
          122 => 'modules/node/views_handler_filter_node_type.inc',
          123 => 'modules/node/views_handler_filter_node_uid_revision.inc',
          124 => 'modules/node/views_plugin_argument_default_node.inc',
          125 => 'modules/node/views_plugin_argument_validate_node.inc',
          126 => 'modules/node/views_plugin_row_node_rss.inc',
          127 => 'modules/node/views_plugin_row_node_view.inc',
          128 => 'modules/profile/views_handler_field_profile_date.inc',
          129 => 'modules/profile/views_handler_field_profile_list.inc',
          130 => 'modules/profile/views_handler_filter_profile_selection.inc',
          131 => 'modules/search/views_handler_argument_search.inc',
          132 => 'modules/search/views_handler_field_search_score.inc',
          133 => 'modules/search/views_handler_filter_search.inc',
          134 => 'modules/search/views_handler_sort_search_score.inc',
          135 => 'modules/search/views_plugin_row_search_view.inc',
          136 => 'modules/statistics/views_handler_field_accesslog_path.inc',
          137 => 'modules/system/views_handler_argument_file_fid.inc',
          138 => 'modules/system/views_handler_field_file.inc',
          139 => 'modules/system/views_handler_field_file_extension.inc',
          140 => 'modules/system/views_handler_field_file_filemime.inc',
          141 => 'modules/system/views_handler_field_file_uri.inc',
          142 => 'modules/system/views_handler_field_file_status.inc',
          143 => 'modules/system/views_handler_filter_file_status.inc',
          144 => 'modules/taxonomy/views_handler_argument_taxonomy.inc',
          145 => 'modules/taxonomy/views_handler_argument_term_node_tid.inc',
          146 => 'modules/taxonomy/views_handler_argument_term_node_tid_depth.inc',
          147 => 'modules/taxonomy/views_handler_argument_term_node_tid_depth_modifier.inc',
          148 => 'modules/taxonomy/views_handler_argument_vocabulary_vid.inc',
          149 => 'modules/taxonomy/views_handler_argument_vocabulary_machine_name.inc',
          150 => 'modules/taxonomy/views_handler_field_taxonomy.inc',
          151 => 'modules/taxonomy/views_handler_field_term_node_tid.inc',
          152 => 'modules/taxonomy/views_handler_field_term_link_edit.inc',
          153 => 'modules/taxonomy/views_handler_filter_term_node_tid.inc',
          154 => 'modules/taxonomy/views_handler_filter_term_node_tid_depth.inc',
          155 => 'modules/taxonomy/views_handler_filter_vocabulary_vid.inc',
          156 => 'modules/taxonomy/views_handler_filter_vocabulary_machine_name.inc',
          157 => 'modules/taxonomy/views_handler_relationship_node_term_data.inc',
          158 => 'modules/taxonomy/views_plugin_argument_validate_taxonomy_term.inc',
          159 => 'modules/taxonomy/views_plugin_argument_default_taxonomy_tid.inc',
          160 => 'modules/tracker/views_handler_argument_tracker_comment_user_uid.inc',
          161 => 'modules/tracker/views_handler_filter_tracker_comment_user_uid.inc',
          162 => 'modules/tracker/views_handler_filter_tracker_boolean_operator.inc',
          163 => 'modules/system/views_handler_filter_system_type.inc',
          164 => 'modules/translation/views_handler_argument_node_tnid.inc',
          165 => 'modules/translation/views_handler_field_node_link_translate.inc',
          166 => 'modules/translation/views_handler_field_node_translation_link.inc',
          167 => 'modules/translation/views_handler_filter_node_tnid.inc',
          168 => 'modules/translation/views_handler_filter_node_tnid_child.inc',
          169 => 'modules/translation/views_handler_relationship_translation.inc',
          170 => 'modules/user/views_handler_argument_user_uid.inc',
          171 => 'modules/user/views_handler_argument_users_roles_rid.inc',
          172 => 'modules/user/views_handler_field_user.inc',
          173 => 'modules/user/views_handler_field_user_language.inc',
          174 => 'modules/user/views_handler_field_user_link.inc',
          175 => 'modules/user/views_handler_field_user_link_cancel.inc',
          176 => 'modules/user/views_handler_field_user_link_edit.inc',
          177 => 'modules/user/views_handler_field_user_mail.inc',
          178 => 'modules/user/views_handler_field_user_name.inc',
          179 => 'modules/user/views_handler_field_user_permissions.inc',
          180 => 'modules/user/views_handler_field_user_picture.inc',
          181 => 'modules/user/views_handler_field_user_roles.inc',
          182 => 'modules/user/views_handler_filter_user_current.inc',
          183 => 'modules/user/views_handler_filter_user_name.inc',
          184 => 'modules/user/views_handler_filter_user_permissions.inc',
          185 => 'modules/user/views_handler_filter_user_roles.inc',
          186 => 'modules/user/views_plugin_argument_default_current_user.inc',
          187 => 'modules/user/views_plugin_argument_default_user.inc',
          188 => 'modules/user/views_plugin_argument_validate_user.inc',
          189 => 'modules/user/views_plugin_row_user_view.inc',
          190 => 'plugins/views_plugin_access.inc',
          191 => 'plugins/views_plugin_access_none.inc',
          192 => 'plugins/views_plugin_access_perm.inc',
          193 => 'plugins/views_plugin_access_role.inc',
          194 => 'plugins/views_plugin_argument_default.inc',
          195 => 'plugins/views_plugin_argument_default_php.inc',
          196 => 'plugins/views_plugin_argument_default_fixed.inc',
          197 => 'plugins/views_plugin_argument_default_raw.inc',
          198 => 'plugins/views_plugin_argument_validate.inc',
          199 => 'plugins/views_plugin_argument_validate_numeric.inc',
          200 => 'plugins/views_plugin_argument_validate_php.inc',
          201 => 'plugins/views_plugin_cache.inc',
          202 => 'plugins/views_plugin_cache_none.inc',
          203 => 'plugins/views_plugin_cache_time.inc',
          204 => 'plugins/views_plugin_display.inc',
          205 => 'plugins/views_plugin_display_attachment.inc',
          206 => 'plugins/views_plugin_display_block.inc',
          207 => 'plugins/views_plugin_display_default.inc',
          208 => 'plugins/views_plugin_display_embed.inc',
          209 => 'plugins/views_plugin_display_extender.inc',
          210 => 'plugins/views_plugin_display_feed.inc',
          211 => 'plugins/views_plugin_display_page.inc',
          212 => 'plugins/views_plugin_exposed_form_basic.inc',
          213 => 'plugins/views_plugin_exposed_form.inc',
          214 => 'plugins/views_plugin_exposed_form_input_required.inc',
          215 => 'plugins/views_plugin_localization_core.inc',
          216 => 'plugins/views_plugin_localization.inc',
          217 => 'plugins/views_plugin_localization_none.inc',
          218 => 'plugins/views_plugin_pager.inc',
          219 => 'plugins/views_plugin_pager_full.inc',
          220 => 'plugins/views_plugin_pager_mini.inc',
          221 => 'plugins/views_plugin_pager_none.inc',
          222 => 'plugins/views_plugin_pager_some.inc',
          223 => 'plugins/views_plugin_query.inc',
          224 => 'plugins/views_plugin_query_default.inc',
          225 => 'plugins/views_plugin_row.inc',
          226 => 'plugins/views_plugin_row_fields.inc',
          227 => 'plugins/views_plugin_row_rss_fields.inc',
          228 => 'plugins/views_plugin_style.inc',
          229 => 'plugins/views_plugin_style_default.inc',
          230 => 'plugins/views_plugin_style_grid.inc',
          231 => 'plugins/views_plugin_style_list.inc',
          232 => 'plugins/views_plugin_style_jump_menu.inc',
          233 => 'plugins/views_plugin_style_mapping.inc',
          234 => 'plugins/views_plugin_style_rss.inc',
          235 => 'plugins/views_plugin_style_summary.inc',
          236 => 'plugins/views_plugin_style_summary_jump_menu.inc',
          237 => 'plugins/views_plugin_style_summary_unformatted.inc',
          238 => 'plugins/views_plugin_style_table.inc',
          239 => 'tests/handlers/views_handler_area_text.test',
          240 => 'tests/handlers/views_handler_argument_null.test',
          241 => 'tests/handlers/views_handler_argument_string.test',
          242 => 'tests/handlers/views_handler_field.test',
          243 => 'tests/handlers/views_handler_field_boolean.test',
          244 => 'tests/handlers/views_handler_field_custom.test',
          245 => 'tests/handlers/views_handler_field_counter.test',
          246 => 'tests/handlers/views_handler_field_date.test',
          247 => 'tests/handlers/views_handler_field_file_size.test',
          248 => 'tests/handlers/views_handler_field_math.test',
          249 => 'tests/handlers/views_handler_field_url.test',
          250 => 'tests/handlers/views_handler_field_xss.test',
          251 => 'tests/handlers/views_handler_filter_combine.test',
          252 => 'tests/handlers/views_handler_filter_date.test',
          253 => 'tests/handlers/views_handler_filter_equality.test',
          254 => 'tests/handlers/views_handler_filter_in_operator.test',
          255 => 'tests/handlers/views_handler_filter_numeric.test',
          256 => 'tests/handlers/views_handler_filter_string.test',
          257 => 'tests/handlers/views_handler_sort_random.test',
          258 => 'tests/handlers/views_handler_sort_date.test',
          259 => 'tests/handlers/views_handler_sort.test',
          260 => 'tests/test_plugins/views_test_plugin_access_test_dynamic.inc',
          261 => 'tests/test_plugins/views_test_plugin_access_test_static.inc',
          262 => 'tests/test_plugins/views_test_plugin_style_test_mapping.inc',
          263 => 'tests/plugins/views_plugin_display.test',
          264 => 'tests/styles/views_plugin_style_jump_menu.test',
          265 => 'tests/styles/views_plugin_style.test',
          266 => 'tests/styles/views_plugin_style_base.test',
          267 => 'tests/styles/views_plugin_style_mapping.test',
          268 => 'tests/styles/views_plugin_style_unformatted.test',
          269 => 'tests/views_access.test',
          270 => 'tests/views_analyze.test',
          271 => 'tests/views_basic.test',
          272 => 'tests/views_argument_default.test',
          273 => 'tests/views_argument_validator.test',
          274 => 'tests/views_exposed_form.test',
          275 => 'tests/field/views_fieldapi.test',
          276 => 'tests/views_glossary.test',
          277 => 'tests/views_groupby.test',
          278 => 'tests/views_handlers.test',
          279 => 'tests/views_module.test',
          280 => 'tests/views_pager.test',
          281 => 'tests/views_plugin_localization_test.inc',
          282 => 'tests/views_translatable.test',
          283 => 'tests/views_query.test',
          284 => 'tests/views_upgrade.test',
          285 => 'tests/views_test.views_default.inc',
          286 => 'tests/comment/views_handler_argument_comment_user_uid.test',
          287 => 'tests/comment/views_handler_filter_comment_user_uid.test',
          288 => 'tests/node/views_node_revision_relations.test',
          289 => 'tests/taxonomy/views_handler_relationship_node_term_data.test',
          290 => 'tests/user/views_handler_field_user_name.test',
          291 => 'tests/user/views_user_argument_default.test',
          292 => 'tests/user/views_user_argument_validate.test',
          293 => 'tests/user/views_user.test',
          294 => 'tests/views_cache.test',
          295 => 'tests/views_view.test',
          296 => 'tests/views_ui.test',
        ),
        'version' => '7.x-3.6',
        'project' => 'views',
        'datestamp' => '1363810217',
      ),
      'project' => 'views',
      'version' => '7.x-3.6',
    ),
    'views_bulk_operations' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/sites/all/modules/views_bulk_operations/views_bulk_operations.module',
      'name' => 'views_bulk_operations',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views Bulk Operations',
        'description' => 'Provides a way of selecting multiple rows and applying operations to them.',
        'dependencies' => 
        array (
          0 => 'entity',
          1 => 'views',
        ),
        'package' => 'Views',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'plugins/operation_types/base.class.php',
          1 => 'views/views_bulk_operations_handler_field_operations.inc',
        ),
        'version' => '7.x-3.1',
        'project' => 'views_bulk_operations',
        'datestamp' => '1354500015',
        'php' => '5.2.4',
      ),
      'project' => 'views_bulk_operations',
      'version' => '7.x-3.1',
    ),
    'views_content' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/ctools/views_content/views_content.module',
      'name' => 'views_content',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views content panes',
        'description' => 'Allows Views content to be used in Panels, Dashboard and other modules which use the CTools Content API.',
        'package' => 'Chaos tool suite',
        'dependencies' => 
        array (
          0 => 'ctools',
          1 => 'views',
        ),
        'core' => '7.x',
        'files' => 
        array (
          0 => 'plugins/views/views_content_plugin_display_ctools_context.inc',
          1 => 'plugins/views/views_content_plugin_display_panel_pane.inc',
          2 => 'plugins/views/views_content_plugin_style_ctools_context.inc',
        ),
        'version' => '7.x-1.3',
        'project' => 'ctools',
        'datestamp' => '1365013512',
        'php' => '5.2.4',
      ),
      'project' => 'ctools',
      'version' => '7.x-1.3',
    ),
    'views_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/views/tests/views_test.module',
      'name' => 'views_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views Test',
        'description' => 'Test module for Views.',
        'package' => 'Views',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'views',
        ),
        'hidden' => true,
        'version' => '7.x-3.6',
        'project' => 'views',
        'datestamp' => '1363810217',
        'php' => '5.2.4',
      ),
      'project' => 'views',
      'version' => '7.x-3.6',
    ),
    'views_ui' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/profiles/gdp_profile_v1_0/modules/contrib/views/views_ui.module',
      'name' => 'views_ui',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views UI',
        'description' => 'Administrative interface to views. Without this module, you cannot create or edit your views.',
        'package' => 'Views',
        'core' => '7.x',
        'configure' => 'admin/structure/views',
        'dependencies' => 
        array (
          0 => 'views',
        ),
        'files' => 
        array (
          0 => 'views_ui.module',
          1 => 'plugins/views_wizard/views_ui_base_views_wizard.class.php',
        ),
        'version' => '7.x-3.6',
        'project' => 'views',
        'datestamp' => '1363810217',
        'php' => '5.2.4',
      ),
      'project' => 'views',
      'version' => '7.x-3.6',
    ),
    'xmlrpc_test' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/modules/simpletest/tests/xmlrpc_test.module',
      'name' => 'xmlrpc_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'XML-RPC Test',
        'description' => 'Support module for XML-RPC tests according to the validator1 specification.',
        'package' => 'Testing',
        'version' => '7.22',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
  ),
  'themes' => 
  array (
    'bartik' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/themes/bartik/bartik.info',
      'name' => 'bartik',
      'type' => 'theme',
      'owner' => 'themes/engines/phptemplate/phptemplate.engine',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Bartik',
        'description' => 'A flexible, recolorable theme with many regions.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'css/layout.css',
            1 => 'css/style.css',
            2 => 'css/colors.css',
          ),
          'print' => 
          array (
            0 => 'css/print.css',
          ),
        ),
        'regions' => 
        array (
          'header' => 'Header',
          'help' => 'Help',
          'page_top' => 'Page top',
          'page_bottom' => 'Page bottom',
          'highlighted' => 'Highlighted',
          'featured' => 'Featured',
          'content' => 'Content',
          'sidebar_first' => 'Sidebar first',
          'sidebar_second' => 'Sidebar second',
          'triptych_first' => 'Triptych first',
          'triptych_middle' => 'Triptych middle',
          'triptych_last' => 'Triptych last',
          'footer_firstcolumn' => 'Footer first column',
          'footer_secondcolumn' => 'Footer second column',
          'footer_thirdcolumn' => 'Footer third column',
          'footer_fourthcolumn' => 'Footer fourth column',
          'footer' => 'Footer',
        ),
        'settings' => 
        array (
          'shortcut_module_link' => '0',
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'garland' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/themes/garland/garland.info',
      'name' => 'garland',
      'type' => 'theme',
      'owner' => 'themes/engines/phptemplate/phptemplate.engine',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Garland',
        'description' => 'A multi-column theme which can be configured to modify colors and switch between fixed and fluid width layouts.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'style.css',
          ),
          'print' => 
          array (
            0 => 'print.css',
          ),
        ),
        'settings' => 
        array (
          'garland_width' => 'fluid',
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'seven' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/themes/seven/seven.info',
      'name' => 'seven',
      'type' => 'theme',
      'owner' => 'themes/engines/phptemplate/phptemplate.engine',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Seven',
        'description' => 'A simple one-column, tableless, fluid width administration theme.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'stylesheets' => 
        array (
          'screen' => 
          array (
            0 => 'reset.css',
            1 => 'style.css',
          ),
        ),
        'settings' => 
        array (
          'shortcut_module_link' => '1',
        ),
        'regions' => 
        array (
          'content' => 'Content',
          'help' => 'Help',
          'page_top' => 'Page top',
          'page_bottom' => 'Page bottom',
          'sidebar_first' => 'First sidebar',
        ),
        'regions_hidden' => 
        array (
          0 => 'sidebar_first',
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
    'stark' => 
    array (
      'filename' => '/var/aegir/platforms/drupal-7.22/themes/stark/stark.info',
      'name' => 'stark',
      'type' => 'theme',
      'owner' => 'themes/engines/phptemplate/phptemplate.engine',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Stark',
        'description' => 'This theme demonstrates Drupal\'s default HTML markup and CSS styles. To learn how to build your own theme and override Drupal\'s default code, see the <a href="http://drupal.org/theme-guide">Theming Guide</a>.',
        'package' => 'Core',
        'version' => '7.22',
        'core' => '7.x',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'layout.css',
          ),
        ),
        'project' => 'drupal',
        'datestamp' => '1365027012',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.22',
    ),
  ),
);
$options['site_ip_addresses'] = array (
  '@server_master' => '192.168.1.100',
);
$options['installed'] = true;
$options['profile'] = 'gdp_profile_v1_0';
$options['language'] = 'en';
$options['aliases'] = array (
);
# Aegir additions
$_SERVER['db_type'] = $options['db_type'];
$_SERVER['db_port'] = $options['db_port'];
$_SERVER['db_host'] = $options['db_host'];
$_SERVER['db_user'] = $options['db_user'];
$_SERVER['db_passwd'] = $options['db_passwd'];
$_SERVER['db_name'] = $options['db_name'];
